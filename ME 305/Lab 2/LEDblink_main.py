# -*- coding: utf-8 -*-
"""
Created on Sat Oct 10 17:23:02 2020

@file           LEDblink_main.py
@author         Miles Young
@date           10/10/2020
@brief          Main file into which LED blinker FSMs are imported and run.
                Source Code at https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%202/LEDblink_main.py
"""

from LED_FSM import LEDBlink

## LED blink task object with frequency of 0.1 MHz or interval of 10 ms 
blinktask_1 = LEDBlink(0.1)

# Run blinking tasks

for N in range(500):
    blinktask_1.virtBlink()

for N in range(500):
    blinktask_1.sinBlink()

for N in range(500):
    blinktask_1.sawtoothBlink()
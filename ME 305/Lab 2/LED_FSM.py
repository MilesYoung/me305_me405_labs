# -*- coding: utf-8 -*-
"""
Created on Tue Oct  6 09:20:56 2020

@file       LED_FSM.py
@author     Miles Young
Date        10/06/2020
@brief      Script which blinks an LED using different patterns
@details    This finite state machine either blinks a virtual LED ON and OFF over fixed intervals,
            or else it causes a physical LED on the Nucleo L476RG to blink according to either a 
            sinusoidal or sawtooth wave.
            Source code at https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%202/LED_FSM.py
            
"""

import utime
import pyb
import math

class LEDBlink:
    '''
    @brief      Class for LED blinking task
    '''
    
    ## The initial state at which the task begins upon running
    S0_init = 0
    
    ## The first state during which the voltage supplied to the LED is set 
    S1_LED_inc = 1
    
    ## The second state during which the LED voltage is incrementally set again
    S2_LED_inc = 2
    
    
    def __init__(self,frequency):
        '''
        @brief              Constructs the LED
        @param frequency    Defines the frequency of the LED "blink" in kHz
        '''
        
        ## Defines the initial state for the task
        self.state = self.S0_init
        
        ## Input that defines the blink frequency
        self.frequency = frequency
        
        ## Convert input frequency to period in ms for timekeeping purposes
        self.period = 1/self.frequency
        
        ## Defines incremental step as 1/100th of the period
        self.increment = self.period/100
        
        ## Defines a constant interval for angle increase 
        self.ang_int = 2*math.pi/100
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## Defines a variable representing the duty cycle for PWM
        self.PWM = 0
        
        ## Defines a variable representing the angle used in cosine calculations
        self.angle = 0
        
        ## The timestamp for the initial iteration in milliseconds
        self.startTime = utime.ticks_ms()
        
        ## Time for which next iteration will run
        self.nextTime = utime.ticks_add(self.startTime,int(self.increment))
        
        ## Set up LED pin on MCU
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
        
        ## Set up timer for PWM
        self.tim2 = pyb.Timer(2,freq = 20000)
        
        ## Set up channel through which PWM is controlled
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin = self.pinA5)
        
        
    def virtBlink(self):
        '''
        @brief          Runs a blinking task for a virtual LED
        @image html virtBlink_FSM_Diagram.png
        '''
        ## Updates to the current time recorded by the controller clock
        self.currTime = utime.ticks_ms()
        if utime.ticks_diff(self.currTime,self.nextTime) >= 0:
            print('run ' + str(self.runs) + ', time ' + str(round((self.currTime-self.startTime)/1000,2)) + ' ms')
            
            
            if(self.state == self.S0_init):
                # Run state 0 initialization
                self.transitionTo(self.S1_LED_inc)
                print('LED off')
                
            elif(self.state == self.S1_LED_inc):                
                # Run state 1 incrementor code               
                self.transitionTo(self.S2_LED_inc)
                print('LED on')

                
            elif(self.state == self.S2_LED_inc):
                # Run state 2 incrementor code              
                self.transitionTo(self.S1_LED_inc)
                print('LED off')
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.nextTime = utime.ticks_add(self.nextTime,int(self.increment))
            
            #Put a dividing space between each run's printed output
            print('')
        
        
        
    def sinBlink(self):
        '''
        @brief         Runs a blinking task according to a sine wave function
        @image html sinBlink_FSM_Diagram.png
        '''
        
        self.currTime = utime.ticks_ms()
        if utime.ticks_diff(self.currTime,self.nextTime) >= 0:
            print('run ' + str(self.runs) + ', time ' + str(round((self.currTime-self.startTime)/1000,2)) + ' ms')
            
            
            if(self.state == self.S0_init):
                # Run state 0 initialization
                self.transitionTo(self.S1_LED_inc)
                # Reset LED PWM
                self.PWM = 100
                self.angle = 0
                
            elif(self.state == self.S1_LED_inc):                
                # Run state 1 incrementor code               
                self.transitionTo(self.S2_LED_inc)
                # Incrementally increase PWM duty cycle
                self.angle += self.ang_int
                self.PWM = 50 * math.cos(self.angle) + 50

                
            elif(self.state == self.S2_LED_inc):
                # Run state 2 incrementor code              
                self.transitionTo(self.S1_LED_inc)
                # Incrementally increase PWM duty cycle
                self.angle += self.ang_int
                self.PWM = 50 * math.cos(self.angle) + 50
                
            # Check to see if end of cycle has been reached, then resets back to initial state  
            if(self.angle >= 2*math.pi):
                self.transitionTo(self.S0_init)
                
            print('PWM Duty Cycle: ' + str(round(self.PWM)) + '%')                
            self.t2ch1.pulse_width_percent(self.PWM)
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.nextTime = utime.ticks_add(self.nextTime,int(self.increment))
            
            #Put a dividing space between each run's printed output
            print('')
        
    def sawtoothBlink(self):
        '''
        @brief          Runs a blinking task according to a sawtooth wave function
        @image html sawtoothBlink_FSM_Diagram.png
        '''
        
        self.currTime = utime.ticks_ms()
        if utime.ticks_diff(self.currTime,self.nextTime) >= 0:
            print('run ' + str(self.runs) + ', time ' + str(round((self.currTime-self.startTime)/1000,2)) + ' ms')
            
            
            if(self.state == self.S0_init):
                # Run state 0 initialization
                # Reset LED PWM
                self.PWM = 0
                self.transitionTo(self.S1_LED_inc)
                
            elif(self.state == self.S1_LED_inc):                
                # Run state 1 incrementor code               
                self.transitionTo(self.S2_LED_inc)
                # Incrementally increase PWM duty cycle
                self.PWM += 1

                
            elif(self.state == self.S2_LED_inc):
                # Run state 2 incrementor code              
                self.transitionTo(self.S1_LED_inc)
                self.PWM += 1
                
            # Check to see if end of cycle has been reached, then resets back to initial state     
            if(self.PWM >= 100):
                self.transitionTo(self.S0_init)
                
            print('PWM Duty Cycle: ' + str(self.PWM) + '%')                
            self.t2ch1.pulse_width_percent(self.PWM)
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.nextTime = utime.ticks_add(self.nextTime,int(self.increment))
            
            #Put a dividing space between each run's printed output
            print('')
        
    
    def transitionTo(self,newState):
        '''
        @brief              Transitions between states
        @param newState     The desired state for next iteration        
        '''
        
        self.state = newState
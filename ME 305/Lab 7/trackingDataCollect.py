# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 16:17:26 2020

@file               trackingDataCollect.py
@date               11/23/2020
@author             Miles Young
@brief              A task class for handling motor speed data collection.\n
@details            This task class is responsible for recieving commands from 
                    the user front-end on the PC via serial communication, including
                    commands to set the closed-loop controller proportional gain
                    to specific values as well as commands to begin and end data
                    collection. This task also populates arrays for time, measured
                    motor speed, reference motor speed, measured motor position, and reference motor position and returns them to 
                    the PC via serial communication. In order to aid in tuning the system, reference data has been scaled by a factor of 10.\n
                    Sourcecode: https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%207/trackingDataCollect.py
"""

from pyb import UART
import struct
import uarray
import utime
import math
import MotorShared

class DataCollect:
    '''
    @brief      Motor Controller data collection class
    @details    This class consists of a constructor, run() method, and
                transitionTo(newState) method. The run() method is a FSM which
                has an initialization state, a state which waits for the command
                to begin data collection and also prepares empty arrays and 
                time limits, and a data collection state which also waits for 
                command to terminate data collection early.
    @image html motor_DataCollect_FSM_Diagram_Lab6.png
    '''
    
    ## Initial state of data collection task FSM
    S0_init = 0
    
    ## First state of data collection task FSM in which shared values and measured data arrays are cleared/reset
    S1_reset = 1
    
    ## Second state of data collection task FSM in which controller gain Kp is recieved from PC and set in shared file
    S2_setKp = 2
    
    ## Third state of data collection task FSM in which controller gain Ki is recieved from PC and set in shared file
    S3_setKi = 3
    
    ## Fourth state of data collection task FSM which waits for character input from user through UART
    S4_initiateCollect = 4
    
    ## Fifth state of data collection task FSM which passes character input to user interface front end through UART
    S5_handleCollect = 5

    def __init__(self,taskNum,runtime):
        '''
        @brief              Construct the user interface
        @param taskNum      A number to identify the task
        @param runtime      The desired data collection runtime, which must be
                            the same as that designated in the front end UI task on the PC
        '''
        
        ## Defines the starting state for the run() method
        self.state = self.S0_init
        
        ## Defines the number of the task based on constructor input
        self.taskNum = taskNum
        
        ## Defines variable to hold command from REPL
        self.cmd = None
        
        ## Defines a variable to hold response from controller
        self.resp = None
        
        ## The timestamp for the initial iteration in milliseconds
        self.startTime = utime.ticks_ms()
        
        ## Defines the current time for the iteration and is overwritten at the beginning of each iteration
        self.currTime = utime.ticks_ms()
        
        ## Defines the interval after which another iteration will run as (pulses/PPS)
        self.interval = 25
        
        ## Creates an empty array to hold time
        self.timeArray = uarray.array('i')
        
        ## Creates an empty array to hold motor speed
        self.omegaArray  = uarray.array('f')
        
        ## Creates an empty array to hold reference speed
        self.omegaRefArray = uarray.array('f',602*[0])
        
        ## Creates an empty array to hold measured motor position
        self.thetaArray = uarray.array('f')
        
        ## Creates an empty array to hold reference motor position
        self.thetaRefArray = uarray.array('f',602*[0])
        
        ## Define the length of the runtime based according to user specification
        self.runtime = runtime
        
        ## Time for which next iteration will run and is overwritten at the end of each iteration
        self.nextTime = utime.ticks_add(self.startTime,self.interval)
        
        ## Holds the time at which data collection must end in milliseconds + the time interval between when the variable is defined in state 1 and when data collection will begin in state 2. Note that in order to avoid running too long the current time is rounded down to the nearest 100th place
        self.finTime = utime.ticks_add(utime.ticks_add(self.interval*math.floor(self.currTime/self.interval),self.runtime),self.interval)
        
        ## Creates a variable to hold the index of the current iteration of the task
        self.runs = 0
        
        ## Defines the serial port for communication with the controller
        self.myuart = UART(2)
        
    def run(self):
        '''
        @brief          Runs one iteration of the data collection task
        '''
    
        ## Updates to the current time recorded by the controller clock
        self.currTime = utime.ticks_ms()
        
        # Specifying the next time the task will run
        if utime.ticks_diff(self.currTime, self.nextTime) >= 0:
            # If the interval has been reached
    
            # SO opens serial port
            if(self.state == self.S0_init):
                # Run state 0 code
                # Transition to next state
                self.transitionTo(self.S1_reset)
                
            
            elif(self.state == self.S1_reset):
                # Clear arrays and shared variables
                MotorShared.begin = False
                MotorShared.end = False
                MotorShared.omega = 0
                MotorShared.omegaRef = 0
                MotorShared.theta = 0
                MotorShared.thetaRef = 0
                self.timeArray = uarray.array('i')
                self.omegaArray = uarray.array('f')
                self.thetaArray = uarray.array('f')
                self.transitionTo(self.S2_setKp)
                #self.transitionTo(self.S4_initiateCollect)
                
            elif(self.state == self.S2_setKp):
                if self.myuart.any() != 0:
                    # Set proportional gain according to input from user front end
                    self.cmd = self.myuart.read()
                    #print(str(self.cmd))
                    MotorShared.Kp = struct.unpack('f', self.cmd)[0]
                    self.cmd = None
                    print('New Kp set as: ' + str(MotorShared.Kp))
                    self.transitionTo(self.S3_setKi)
                else:
                    # Remain in current state until command is recieved
                    pass  
            
            elif(self.state == self.S3_setKi):
                if self.myuart.any() != 0:
                    # Set proportional gain according to input from user front end
                    self.cmd = self.myuart.read()
                    #print(str(self.cmd))
                    MotorShared.Ki = struct.unpack('f', self.cmd)[0]
                    self.cmd = None
                    print('New Ki set as: ' + str(MotorShared.Ki))
                    self.transitionTo(self.S4_initiateCollect)
                else:
                    # Remain in current state until command is recieved
                    pass        
            
            elif(self.state == self.S4_initiateCollect):
                # Run state 1 code
                if self.myuart.any() != 0:
                    # Read command
                    self.cmd = self.myuart.read()
                    # Clear command
                    self.cmd = None
                    # Send response
                    self.resp = 'Beginning data collection'
                    print(self.resp)
                    # Clear response
                    self.resp = None
                    # Notify controller to begin
                    MotorShared.begin = True                    
                    # Define the time at which data collection will automatically terminate. This is the current time + 10 seconds + the time interval between the current time and when the data collection will begin
                    self.finTime = utime.ticks_add(self.interval*math.floor(self.currTime/self.interval),self.runtime) + self.interval
                    # Transition to next state
                    self.transitionTo(self.S5_handleCollect)
                else:
                    # Remain in current state until command is recieved
                    pass
                    
            elif(self.state == self.S5_handleCollect):
                # Run state 2 code
                
                # Add updated time to time array
                self.timeArray.append(int(self.interval*len(self.timeArray)))
                # Add updated motor speed to motor speed array
                self.omegaArray.append(MotorShared.omega)
                self.thetaArray.append(MotorShared.theta)
                
                # If there is a command waiting in the bus
                if self.myuart.any() != 0:
                    # Checks for character in bus
                    self.cmd = self.myuart.readchar()
                    if(self.cmd == 115):
                        # If immediate termination is requested:
                        # Clear command
                        self.cmd = None
                        # Return to previous state
                        self.transitionTo(self.S1_reset)
                        # Notify user that data collection has been terminated early
                        self.resp = 'Terminating data collection early'
                        print(self.resp)
                        # Clear response
                        self.resp = None
                        # Notify controller to end early
                        MotorShared.end = True
                        # Send data collection to PC
                        for k in range(len(self.timeArray)):
                            print('{:},{:},{:},{:},{:}'.format(self.timeArray[k],self.omegaArray[k],self.omegaRefArray[k],self.thetaArray[k],self.thetaRefArray[k]))
                    else:
                        # If incorrect command is sent, update should still occur
                        self.cmd = None
                        # Update motor speed in shared file
                        MotorShared.omegaRef = self.omegaRefArray[len(self.timeArray)]
                else:
                    # This part of the code is where the actual data collection occurs
                    # Remain in current state until command is received or data collection is automatically terminated
                    if utime.ticks_diff(self.currTime,self.finTime) < 0:
                        # If runtime has not been reached
                        # Send current reference speed to shared
                        MotorShared.omegaRef = self.omegaRefArray[len(self.timeArray)]
                    elif utime.ticks_diff(self.currTime,self.finTime) >= 0:
                        # If runtime has been reached:
                        # Return to previous state
                        self.transitionTo(self.S1_reset)
                        # Notify user that data collection is complete
                        self.resp = 'Data collection complete'
                        print(self.resp)
                        # Clear response
                        self.resp = None
                        # Notify controller to end early
                        MotorShared.end = True
                        # Send data collection to PC
                        for k in range(len(self.timeArray)):
                            if k == int(len(self.timeArray)/2):
                                utime.sleep_ms(1010)
                                print('{:},{:.2f},{:.2f},{:.2f},{:.2f}'.format(self.timeArray[k],self.omegaArray[k],self.omegaRefArray[k],self.thetaArray[k],self.thetaRefArray[k]))
                            else:
                                print('{:},{:.2f},{:.2f},{:.2f},{:.2f}'.format(self.timeArray[k],self.omegaArray[k],self.omegaRefArray[k],self.thetaArray[k],self.thetaRefArray[k]))
                                
            # Define time after which the data collection task will commence
            self.nextTime = utime.ticks_add(self.nextTime,int(self.interval))
                
            # Increase run count by 1
            self.runs += 1
    
    def transitionTo(self,newState):
        '''
        @brief          Transitions between states
        @param newState The desired state for next iteration
        '''
        self.state = newState
    
    
    def stepRef(self,refSpeed):
        '''
        @brief          creates a step input for reference speed
        @param refSpeed Desired magnitude of reference speed
        '''
        
        zeros = (1000/self.interval)
        self.omegaRefArray = uarray.array('f', zeros*[0])
        ref = self.runtime - (1000-2*self.interval)
        while ref > 0:
            self.omegaRefArray.append(refSpeed)
            ref = ref - self.interval

    def loadRef(self,refcsv):
        '''
        @brief           Extracts arrays for time, speed, and position from a .csv file and resamples data according to task interval
        @param refcsv    File from which reference time, speed, and position are to be extracted
        '''
        
        ## Opens the file from which data is to be read
        with open(str(refcsv)) as ref:
            
            k = 0
            
            while True:
                line = ref.readline()
                
                if line == '':
                    break
                
                else:
                    (t,v,x) = line.strip().split(',')
                    
                    if float(t) == 0:
                        print('Resampling reference data...')
                        self.omegaRefArray[k] = 10*float(v)
                        self.thetaRefArray[k] = 10*float(x)
                        k += 1
                    elif int(float(t)*1000) % self.interval == 0:
                        print('Resampling reference data...')
                        self.omegaRefArray[k] = 10*float(v)
                        self.thetaRefArray[k] = 10*float(x)
                        k += 1
                    else:
                        pass
                    
        print('Reference data loaded successfully')

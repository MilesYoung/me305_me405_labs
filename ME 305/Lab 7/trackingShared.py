# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 16:37:03 2020

@file               trackingShared.py
@date               11/23/2020
@author             Miles Young
@brief              File for value shared between data collection and closed-loop tasks
@details            Sourcecode: https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%207/trackingShared.py
"""

## Signal to begin data collection
begin = False

## Signal to end data collection
end = False

## Holds controller proportional gain value set by PC front-end
Kp = .625

## Holds controller integral gain value set by PC front-end
Ki = 0.1

## Holds current reference motor speed
omegaRef = 0

## Holds current measured motor speed
omega = 0

## Holds current measured motor position
theta = 0





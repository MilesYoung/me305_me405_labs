# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 16:19:29 2020

@file               trackingFrontUI.py
@date               11/23/2020
@author             Miles Young
@brief              Front-end user interface for controlling motor speed.\n
@details            This code utilizes a finite state machine to send commands to 
                    a controller, either setting closed-loop proporional gain, 
                    or initiating or terminating data collection from a sensor, 
                    and recieve susequent responses. It saves the collected sensor
                    data as a .csv file and plots it as well.\n
                    Sourcecode: https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%207/trackingFrontUI.py
@image html Lab_7_Plot.png
"""

import serial
import array
import time
import struct
import keyboard
import numpy as np
import matplotlib.pyplot as plt

class FrontUI:
    '''
    @brief              User interface front end class
    @details            This class allows a user to interact with a sensor
                        connected to the controller through serial 
                        communication. It can prompt the controller to begin
                        collecting data using a 'g' keystroke and stop the data
                        collection using 's' keystroke. It accepts an array of 
                        data from the controller, which it then plots as well as 
                        converts to a .csv (comma-seperated values) file 
    @image html motor_FrontEnd_FSM_Diagram_Lab6.png
    '''     
    ## Initial state of user interface front end task FSM
    S0_init = 0

    ## Second state of user interface front end task FSM which waits for character input through UART
    S1_begin = 1
    
    ## Third state of user interface front end task FSM which passes character input to controller through UART
    S2_end = 2
    
    ## Final state of user interface front end task FSM which closes the serial port
    S3_close = 3
    
    def __init__(self,taskNum,runtime,dbg = True):
        '''
        @brief          Construct the user interface
        @param taskNum  A number to identify the task
        @param runtime  The desired data collection runtime, which must be the same as that designated for the data collection task on the Nuleo
        @param dbg      A boolean indicating whether the task should print a trace or not
        '''
        
        ## Defines the starting state for the run() method
        self.state = self.S0_init
        
        ## Defines the previous state, used for conditional statement in State 0 initialization
        self.prevstate = self.S0_init
        
        ## Defines the number of the task based on constructor input
        self.taskNum = taskNum
          
        ## Flag to print debug messages or suppress them based on constructor input
        self.dbg = dbg  
        
        ## Creates a variable to hold the index of the current iteration of the task
        self.runs = 0
        
        ## Defines the serial port for communication with the controller
        self.ser = serial.Serial(port = 'COM3',baudrate = 115273,timeout = 1)
        
        ## Defines variable to hold command from REPL
        self.cmd = None
        
        ## Defines a variable to hold resgponse from controller
        self.resp = None
        
        ## Defines a variable to hold the Kp value input by the user when prompted
        self.Kp = float
        
        ## Defines a variable to hold the Ki value input by the user when prompted
        self.Ki = float
        
        ## Defines a list to hold the unformatted string of data recieved from controller
        self.readData = []
        
        ## Defines array to hold properly converted and formatted array of time values
        self.time = array.array('f')
        
        ## Defines array to hold properly converted and formatted measured motor speed values
        self.omega = array.array('f')
        
        ## Defines array to hold properly converted and formatted reference motor speed values
        self.omegaRef = array.array('f')
        
        ## Defines array to hold properly converted and formtatted measured motor position values
        self.theta = array.array('f')
        
        ## Defines array to hold properly converted and formatted reference motor position values
        self.thetaRef = array.array('f')
        
        ## Defines an indicator of whether the FSM should continue to iterate
        self.iterate = True
        
        if self.dbg:
            print('Initiated user interface front end')
        
    def run(self):
        '''
        @brief          Runs one iteration of the user interface front end task
        '''
         
        # SO opens serial port
        if(self.state == self.S0_init):
            # Run state 0 code
            
            # Debugging
            self.printTrace()
            # Transition to state 1
            # Clear any pre-existing characters
            while self.ser.in_waiting > 0:
                self.resp = self.ser.readline().decode('ascii') 
                # Print response
                print(self.resp)
                # Clear response
                self.resp = None
            # Prompt user input of desired controller gain
            self.Kp = float(input('Input desired proportional gain Kp: '))
            if(self.Kp > 0):
                self.ser.write(bytearray(struct.pack('f',self.Kp)))
                # Clear input variable
                # Read response from Nucleo
                self.resp = self.ser.readline().decode('ascii')
                print(str(self.resp))
                # Clear response variable
                self.resp = None
            else:
                print('Please input a positive integer or a floating point value')
            self.Ki = float(input('Input desired integral gain Ki: '))
            if(self.Ki > 0):
                self.ser.write(bytearray(struct.pack('f',self.Ki)))
                # Clear input variable
                # Read response from Nucleo
                self.resp = self.ser.readline().decode('ascii')
                print(str(self.resp))
                # Clear response variable
                self.resp = None
                print("Press 'g' to begin data collection")
                self.transitionTo(self.S1_begin) 
            else:
                print('Please input a positive integer or floating point value')
            
        elif(self.state == self.S1_begin):
            # Run state 1 code 
            # Debugging
            self.printTrace()
            # Prompt user input
            if(keyboard.is_pressed('g')):
                # Send command
                self.ser.write(str('g').encode('ascii'))
                # Clear command
                self.cmd = None
                # Read response
                self.resp = self.ser.readline().decode('ascii')
                print(self.resp)
                # Clear response
                self.resp = None
                # Transition to next state
                print("Press 's' to terminate data collection early")
                self.transitionTo(self.S2_end)
            else:
                # Error handling
                pass
        
            
        elif(self.state == self.S2_end):
            # Run state 2 code
            
            # Debugging
            self.printTrace()
            # Prompt user input
            
            print('Collecting...')
            
            if(self.ser.in_waiting != 0):
                 # Read response
                self.resp = self.ser.readline().decode('ascii')
                print(self.resp)
                # Clear response
                self.resp = None
                # Read, reformat, save, and plot data
                self.processData(self.time,self.omega,self.omegaRef,self.theta,self.thetaRef)
                # Plot speed array
                fig,axs = plt.subplots(2)
                fig.suptitle('Motor Speed & Position vs. Time'+ '\n' + 'Kp = ' + str(self.Kp) + ', ' + 'Ki = ' + str(self.Ki) + ', ' + 'J = ' + '{:.2f}'.format(self.calcJ(self.omegaRef,self.omega,self.thetaRef,self.theta)))
                axs[0].plot(self.time,self.omega,'r',self.time,self.omegaRef,'k')
                axs[0].set_ylabel('Motor Speed [RPM]')
                axs[0].set_xlabel('Time [ms]')
                # Plot position data
                axs[1].plot(self.time,self.theta,'r',self.time,self.thetaRef,'k')
                axs[1].set_ylabel('Motor Position [deg]')
                axs[1].set_xlabel('Time [ms]')
                # Calculate controller performance metric J
                print('Controller Performance Metric,J = ' + '{:.2f}'.format(self.calcJ(self.omegaRef,self.omega,self.thetaRef,self.theta)))
                # Transition to closing state
                self.transitionTo(self.S3_close)
                
                
            elif(keyboard.is_pressed('s')):
                # Send user input
                self.ser.write(str('s').encode('ascii'))
                # Clear command
                self.cmd = None
                # Read response
                self.resp = self.ser.readline().decode('ascii')
                print(self.resp)
                # Clear response
                self.resp = None
                # Read, reformat, save, and plot data
                self.processData(self.time,self.omega,self.omegaRef,self.theta,self.thetaRef)
                # Plot speed array
               # Plot speed array
                fig,axs = plt.subplots(2)
                fig.suptitle('Motor Speed & Position vs. Time'+ '\n' + 'Kp = ' + str(self.Kp) + ', ' + 'Ki = ' + str(self.Ki) + ', ' + 'J = ' + '{:.2f}'.format(self.calcJ(self.omegaRef,self.omega,self.thetaRef,self.theta)))
                axs[0].plot(self.time,self.omega,'r',self.time,self.omegaRef,'k')
                axs[0].set_ylabel('Motor Speed [RPM]')
                axs[0].set_xlabel('Time [ms]')
                # Plot position data
                axs[1].plot(self.time,self.theta,'r',self.time,self.thetaRef,'k')
                axs[1].set_ylabel('Motor Position [deg]')
                axs[1].set_xlabel('Time [ms]')
                # Calculate controller performance metric J
                print('Controller Performance Metric,J = ' + '{:.2f}'.format(self.calcJ(self.omegaRef,self.omega,self.thetaRef,self.theta)))
                # Transition to closing state
                self.transitionTo(self.S3_close)
            else:
                # Error handling
                pass
        
        elif(self.state == self.S3_close):
            # Close serial port communication
            self.ser.close()
            self.iterate = False
            
        self.runs += 1
        
        
    
    def transitionTo(self,newState):
        '''
        @brief          Transitions between states
        @param newState The desired state for next iteration
        '''
        
        self.state = newState
    
    def printTrace(self):
        '''
        @brief          Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}'.format(self.taskNum, self.state, self.runs)
            print(str)
    
    def processData(self,set1,set2,set3,set4,set5):
        '''
        @brief          This method is responsible for reading and interpreting the data sent in string format from the encoder. It strips return and newline statements from strings, splits according to the comma between time and position values, saves the newly combed data as a .csv file, and populates a time and position array which are then plotted.
        '''
        
        for k in range(2):
            #Delay 0.5 seconds to allow transfer of data
            time.sleep(2)
            # Read data array
            while self.ser.in_waiting > 0:
                self.readData.append(self.ser.readline().decode('ascii').strip('\r\n').split(','))
        # Save data array to .csv file
        np.savetxt('Motor Position & Speed.csv',self.readData,delimiter=',',fmt='%s',header='Time [ms], Motor Speed [RPM], Reference Motor Speed [RPM], Motor Position [deg], Reference Motor Position [deg]')
        print('.csv file saved')
        for n in range(len(self.readData)):
            set1.append(float(self.readData[n][0]))
            set2.append(float(self.readData[n][1]))
            set3.append(float(self.readData[n][2]))
            set4.append(float(self.readData[n][3]))
            set5.append(float(self.readData[n][4]))
        # Clears readData array so it can be used again on the next run of this method
        self.readData = []
   
        
    def calcJ(self,referenceSpeed,measuredSpeed,referencePosition,measuredPosition):
                '''
                @brief                      Calculates the performance metric J for the closed loop controller
                @param referenceSpeed       An array holding reference speed data
                @param measuredSpeed        An array holding measured speed data
                @param referencePosition    An array holding reference position data
                @param measuredPosition     An array holding measured position data
                '''
                
                J = float(0)
                K = len(referenceSpeed)
                
                for k in range(K):
                    J = J + ((referenceSpeed[k] - measuredSpeed[k])**2 - (referencePosition[k] - measuredPosition[k])**2)
                
                J = J/K
                
                return J
        
# Run User Interface Front End

## Define runtime
runtime = 10000

## Create user interface task
UserObject = FrontUI(1,runtime,dbg = False)

while UserObject.iterate == True:
    UserObject.run()

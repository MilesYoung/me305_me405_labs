# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 13:24:09 2020

@file           tracking_main.py
@author         Miles Young
@date           11/10/2020
@brief          Main file for the motor.\n
@details        This file defines all required pin objects for two motors to function. It then creates two motor objects using the MotorDriver class, which can be controlled via REPL.\n
                Sourcecode: https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%207/tracking_main.py
@image html motor_Task_Diagram_Lab6.png
"""

import pyb
from MotorDriver import MotorDriver
from MotorEncoder import EncoderDriver
from trackingClosedLoop import ClosedLoopDriver, ClosedLoopTask
from trackingDataCollect import DataCollect

# MOTOR OBJECTS

## Enable/disable pin
pinSleep = pyb.Pin(pyb.Pin.cpu.A15, mode=pyb.Pin.OUT_PP)

## Forward driving pin for motor 1
pinIN1 = pyb.Pin(pyb.Pin.cpu.B4)

## Reverse driving pin for motor 1
pinIN2 = pyb.Pin(pyb.Pin.cpu.B5)

## Forward driving pin for motor 2
pinIN3 = pyb.Pin(pyb.Pin.cpu.B0)

## Reverse driving pin for motor 2
pinIN4 = pyb.Pin(pyb.Pin.cpu.B1)

## Timer number for motor 1
timer1 = 3

## Timer number for motor 2
timer2 = 3

## Timer channel for pinIN1
channel1 = 1

## Timer channel for pinIN2
channel2 = 2

## Timer channel for pinIN3
channel3 = 3

## Timer channel for pinIN4
channel4 = 4

## Motor 1 Object
Motor1 = MotorDriver(1,pinSleep,pinIN1,channel1,pinIN2,channel2,timer1) 

## Motor 2 object
Motor2 = MotorDriver(2,pinSleep,pinIN3,channel3,pinIN4,channel4,timer2)

# ENCODER OBJECTS

## Define pin A1 object
pinA1 = pyb.Pin.cpu.B6

## Define pin B1 object
pinB1 = pyb.Pin.cpu.B7

## Define the timer to be used for encoder 1
timer1 = 4

## Define pin A2 object
pinA2 = pyb.Pin.cpu.C6

## Define pin B2 object
pinB2 = pyb.Pin.cpu.C7

## Define the timer to be used for encoder 2
timer2 = 8

## Define the pulses per cycle for each encoder
PPC = 4

## Define the cycles per rotation for each encoder
CPR = 1000

## Define the gear ratio for each encoder to motor
gearRatio = 4

## Encoder 1 Object
Encoder1 = EncoderDriver(pinA1,pinB1,timer1,PPC,CPR,gearRatio)

## Encoder 2 object
Encoder2 = EncoderDriver(pinA2,pinB2,timer2,PPC,CPR,gearRatio)

# CLOSED LOOP OBJECT AND TASK

## Define starting proportional gain for controller
Kp = .6

## Define starting integral gain for controller
Ki = 0.1

## Closed Loop Controller Object for Motor 1
CLObject1 = ClosedLoopDriver(Kp,Ki)

## Closed Loop Controller Object for Motor 2
CLObject2 = ClosedLoopDriver(Kp,Ki)

## Closed Loop Task for Motor 1
CLTask1 = ClosedLoopTask(1,CLObject1,Encoder1,Motor1)

## Closed loop task for Motor 2
CLTask2 = ClosedLoopTask(2,CLObject2,Encoder2,Motor2)

# DATA COLLECTION TASK

## Desired runtime according to reference.csv time array
runtime = 15000

## Data Collection Object for Motor 1
DataTask1 = DataCollect(1,runtime)

## Data Collection Task for Motor 2
DataTask2 = DataCollect(2,runtime)

# RUNNING TASKS

# Load reference data
DataTask1.loadRef('reference.csv')

# Run code continuously
while True:
    DataTask1.run()
    CLTask1.run()
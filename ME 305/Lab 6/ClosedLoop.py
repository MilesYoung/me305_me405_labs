# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 09:22:29 2020

@file               ClosedLoop.py
@date               11/23/2020
@author             Miles Young
@brief              A closed-loop package containing driver and task classes.\n
@details            This package contains two classes: first, a driver class 
                    which creates an object to calculate an output based on feedback 
                    or set or return the controller proportional gain. And second, 
                    a task class to update the closed-loop controller and share 
                    the updated values into a shared file.\n
                    Sourcecode: https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%206/ClosedLoop.py
"""

import utime
import MotorShared

class ClosedLoopDriver:
    '''
    @brief          This class implements a closed-loop driver for regulating the speed of a DC motor using a P controller
    '''
    def __init__(self,Kp,satMax = 100,satMin = -100):
        '''
        @brief          Creates a closed-loop driver by defining key controller parameters and defining saturation limits
        @param Kp       The initial proportional gain for the closed-loop controller
        @param satMax   The maximum saturation limit for the output PWM level
        @param satMin   The minimum saturation limit for the output PWM level
        '''
        ## The initial proportional gain value as determined by the user in constructor
        self.Kp = Kp
        
        ## Limits maximum saturation levels of PWM
        self.satMax = satMax
        
        ## Limits minimum saturation level of PWM
        self.satMin = satMin
        
    def update(self,inputSignal,feedbackSignal):
        '''
        @brief Updates the duty cycle to be sent to the motor based on calculated error between current motor speed and reference motor speed.
        @param inputSignal      The reference/desired value
        @param feedbackSignal   The measured value
        '''
        ## The calculated error signal
        self.error = self.Kp*(inputSignal-feedbackSignal)        
        
        if self.error >= self.satMin and self.error <= self.satMax:
            return self.error
        elif self.error < self.satMin:
            return self.satMin
        elif self.error > self.satMax:
            return self.satMax
    
    
    def getKp(self):
        '''
        @brief          Returns the closed-loop current controller proportional gain
        '''
        return self.Kp
    
    
    def setKp(self,newKp):
        '''
        @brief          Sets the closed-loop controller proportional gain
        @param newKp    The desired proportional gain 
        '''
        self.Kp = newKp
    
    
    
class ClosedLoopTask:
    '''
    @brief          This class implements a closed-loop task
    @image html motor_ClosedLoop_FSM_Diagram_Lab6.png
    '''
    
    ## initialization state
    S0_init = 0
    
    ## First state of FSM in which task waits for signal to begin feedback control
    S1_wait = 1
    
    ## Second state of FSM in which task facilitates feedback control
    S2_control = 2
    
    
    def __init__(self,taskNum,CLObject,Encoder,MotorObject,interval=20):
        '''
        @brief              Creates a task to run the closed-loop controller 
        @param taskNum      A number identifier for the closed-loop task
        @param interval     The desired interval between each iteration of the task run() method. Defaults to 20 ms.
        @param CLObject     The closed-loop driver object that this task interacts with
        @param Encoder      The encoder object that this task interacts with
        @param MotorObject  The motor object that this task interacts with
        '''
        
        ## Defines the starting state for the run() method
        self.state = self.S0_init
        
        ## Defines the number of the task based on constructor input 
        self.taskNum  = taskNum
        
        ## Defines the interval after which another iteration will run according to user speecification
        self.interval = interval
        
        ## Defines the closed loop object that will be interacted with based on the constructor input
        self.CL = CLObject
        
        ## Defines the encoder object that will be interacted with based on constructor input
        self.Encoder = Encoder
        
        ## Defines the motor onject that will be interacted with based on constructor input
        self.Motor = MotorObject
        
        ## The timestamp for the initial iteration in milliseconds
        self.startTime = utime.ticks_ms()
        
        ## Defines the current time for the iteration and is overwritten at the beginning of each iteration
        self.currTime = utime.ticks_ms()
        
        
        
        ## Time for which next iteration will run and is overwritten at the end of each iteration
        self.nextTime = utime.ticks_add(self.startTime,self.interval)
        
        ## Creates a variable to hold the index of the current iteration of the task
        self.runs = 0
    
        ## Defines the output PWM level of the feedback loop
        self.out = 0
    
    def run(self):
        '''
        @brief          Runs one iteration of the closed loop task
        '''
        
        ## Updates to the current time recorded by the controller clock
        self.currTime = utime.ticks_ms()
        
        # Specifying the next time the task will run
        if utime.ticks_diff(self.currTime, self.nextTime) >= 0:
            # If the interval has been reached
            if self.state == self.S0_init:
                # Run state 0 code
                # Resets the encoder position
                self.Encoder.setPosition(0)
                self.transitionTo(self.S1_wait)
                
            elif self.state == self.S1_wait:
                # Run state 1 code
                if MotorShared.begin == False:
                    # If no signal to begin from data collection task
                    pass
                if MotorShared.begin == True:
                    # If the signal to begin is passed from data collection task
                    self.Motor.enable()
                    # Because shaft may continue to move due to inertia after data collection is complete, it is necessary to udate the position so that error is not introduced before the next round of calculations
                    self.Encoder.update()
                    # Sets the proportional gain of the closed-loop controller object based on the value set by the data collection task
                    self.CL.setKp(MotorShared.Kp)
                    self.transitionTo(self.S2_control)
            
            elif self.state == self.S2_control:
                # Run state 2 code
                if MotorShared.end == False:
                    # If the signal to end data collection has not been passed from the data collection task
                    # Updates the encoder position and calculates difference between previous and current position
                    self.Encoder.update()
                    # Calculates current motor speed, which is shared with data collection task
                    MotorShared.omega = self.Encoder.tick2rpm(self.interval)
                    # Updates PWM level
                    self.out = self.CL.update(MotorShared.omegaRef,MotorShared.omega)
                    if (MotorShared.omegaRef >= 0 and self.out < 0) or (MotorShared.omegaRef <= 0 and self.out > 0):
                        # If motor overshoots
                        self.Motor.brake()
                    else:
                        self.Motor.setDuty(self.out)
                elif MotorShared.end == True:
                    # If the signal to end data collection has been passed from the data collection task
                    while(abs(self.Encoder.getDelta()) > 0):
                        # Brakes the motor to a stop before disabling 
                        self.Encoder.update()
                        self.Motor.brake()
                    # Disables the motor
                    self.Motor.disable()
                    self.transitionTo(self.S0_init)
         
            # Define time after which the data collection task will commence
            self.nextTime = utime.ticks_add(self.nextTime,int(self.interval))
                
            # Increase run count by 1
            self.runs += 1 
                    
                    
    def transitionTo(self,newState):
        '''
        @brief          Transitions between states
        @param newState The desired state for next iteration
        '''
    
        self.state = newState
        
   
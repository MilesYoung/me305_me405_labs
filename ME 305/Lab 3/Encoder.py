# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 08:28:29 2020

@file               Encoder.py
@date               10/13/2020
@author             Miles Young
@brief              A class which interprets encoder output to determine angular position.
@details            Source Code: https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%203/Encoder.py
"""

import shared
import pyb
from pyb import UART
import utime


class EncoderDriver:
    '''
    @brief          A class for intepreting encoder output
    '''
    
    def __init__(self,pinA,pinB,timer,PPC,CPR,gearRatio):
        '''
       @brief           Constructs Encoder class
       @details         This method constructs the Encoder object by assigning
                        pin and timer input parameters and defining initial
                        states of pins
       @param pinA      The pin location for + encoder terminal. must be 
                        properly defined as a pin object before being input
                        into an encoder class. Use pinA = pyb.Pin.cpu.
                        [pin letter(s) and number]
       @param pinB      The pin location for GND encoder terminal. must follow 
                        same format for pinA
       @param timer     The timer number which applies to both pins, which can
                        be found using Table 17 of the Nucleo datasheet
       @param PPC       The pulses per rotation of the encoder
       @param CPR       The cycles per rotation of the encoder
       @param gearRatio The gear ratio between the motor and the output shaft 
                        on which the encoder is mounted
       '''
        
        ## Translates the encoder pin A input into Encoder object 
        self.pinA = pinA
        
        ## Translates the encoder pin B input into Encoder object
        self.pinB = pinB
        
        ## Defines the timer which applies to pins A and B
        self.tim = pyb.Timer(timer, prescaler = 0, period = 0xFFFF) 
        
        ## Constructs channel for pin A within timer
        self.tim.channel(1,pin=self.pinA, mode=pyb.Timer.ENC_AB)
        
        ## Constructs channel for pin B within timer
        self.tim.channel(2,pin=self.pinB, mode=pyb.Timer.ENC_AB)
        
        ## Creates a variable to hold the current position count of the encoder
        self.curr_count = 0
        
        ## Creates a variable to hold the previous position count of the encoder
        self.prev_count = 0
        
        ## Creates a local variable to hold the total position of the encoder
        self.position = 0

        ## Creates a local variable to hold value representing difference between current and previous position
        self.delta = 0 
        
        ## Define the pulses per cycle for this encoder object via user input into constructor
        self.PPC = PPC
        
        ## Define the cycles per revolution for this encoder object via user input into constructor
        self.CPR = CPR
        
        ## Define the gear ratio (motor:encoder) reduced to an integer via user input into the constructor
        self.gearRatio = gearRatio
        
        ## Defines total number of counter values before overflow
        self.overflow = 65536
        

    def update(self):
        '''
        @brief          Updates the encoder position after a set time interval
        '''
        
        # Update current encoder count
        self.curr_count = self.tim.counter()
        # Update difference between previous and current position
        self.delta = self.curr_count - self.prev_count
        
        # Determine if overflow or underflow has occured
        if(abs(self.delta) < (self.overflow/2)):
            self.position = (self.position + self.delta)
        elif(abs(self.delta) >= self.overflow/2):
            if(self.delta < 0):
                # In the case of overflow
                self.position = (self.position + (self.delta + self.overflow))
            elif(self.delta > 0):
                # In the case of underflow
                self.position = (self.position + (self.delta - self.overflow))
        
        # Convert position from ticks to degrees
        self.position = self.tick2deg(self.position)
        
        # Current count becomes previous count for next iteration
        self.prev_count = self.curr_count
        
    def getPosition(self):
        '''
        @brief          Returns the current position of encoder
        '''
       
        return self.position
        
        
    def setPosition(self,newPosition):
        '''
        @brief              Sets the encoder position according to the desired user input parameter 'angle'
        @param newPosition  The angle to which the encoder will be set. must be an integer
        '''
        
        self.position = newPosition
        
        
    def getDelta(self):
        '''
        @brief          Calculates the difference between the current and previous position counts
        '''        
        # Determine if overflow or underflow has occured
        if(abs(self.delta) < (self.overflow/2)):
            return self.delta
        
        elif(abs(self.delta) >= self.overflow/2):
            if(self.delta < 0):
                # In the case of overflow
                return (self.delta + self.overflow)
            
            elif(self.delta > 0):
                # In the case of underflow
                return (self.delta - self.overflow)
    
    def tick2deg(self,ticks):
        '''
        @brief          Converts the position of the encoder from ticks to degrees
        '''
    
        ## Position of encoder in degrees
        theta = ticks*(1/self.PPC)*(1/self.CPR)*(360/1)
    
        return theta
    
    
    
    
class UserInterface:
    '''
    @brief              User interface class
    @details            This class allows a user to interact with the encoder object whilst running
    '''     
    ## Initial state of user interface task FSM
    S0_init = 0
    
    ## First state of user interface task FSM which waits for character input through UART
    S1_char = 1
    
    ## Second state of user interface task FSM which passes character input to controller through UART
    S2_resp = 2
    
    def __init__(self,taskNum,interval,dbg = True):
        '''
        @brief          Construct the user interface
        @param taskNum  A number to identify the task
        @param interval An integer number of microseconds between desired iterations of the task
        @param dbg      A boolean indicating whether the task should print a trace or not
        '''
        
        ## Defines the starting state for the run() method
        self.state = self.S0_init
        
        ## Defines the number of the task based on constructor input
        self.taskNum = taskNum
        
        ## Defines the interval after which another iteration will run based on constructor input
        self.interval = interval
        
        ## Flag to print debug messages or suppress them based on constructor input
        self.dbg = dbg  
        
        ## The timestamp for the initial iteration in milliseconds
        self.startTime = utime.ticks_ms()
        
        ## Defines the interval after which another iteration will run as (pulses/PPS)
        self.interval = interval
        
        ## Time for which next iteration will run
        self.nextTime = utime.ticks_add(self.startTime,int(self.interval))
        
        ## Creates a variable to hold the index of the current iteration of the task
        self.runs = 0
        
        ## Defines the serial port for communication with the controller
        self.ser = UART(2)
        
        if self.dbg:
            print('Created user interface task')
        
    def run(self):
        '''
        @brief          Runs one iteration of the user interface task
        @image html UserInterface_FSM_Diagram.png
        '''
        ## Updates to the current time recorded by the controller clock
        self.currTime = utime.ticks_ms()
        # Specifying the next time the task will run
        if utime.ticks_diff(self.currTime, self.nextTime) >= 0:
            # If the interval has been reached
        
            # SO opens serial port
            if(self.state == self.S0_init):
                self.printTrace()
                # Run state 0 code
                self.transitionTo(self.S1_char)
                
            elif(self.state == self.S1_char):
                self.printTrace()
                # Run state 1 code
                if self.ser.any():
                    self.transitionTo(self.S2_resp)
                    shared.cmd = self.ser.readchar()
                    
            elif(self.state == self.S2_resp):
                self.printTrace()
                # Run state 2 code
                self.transitionTo(self.S1_char)
                shared.resp = None
                print("Options: 'z' for Zero position, 'p' for print Position, or 'd' for print Delta")

            
            else:
                # Invalid state code (error handling)
                pass
            
            self.nextTime = utime.ticks_add(self.nextTime,int(self.interval))
            
            self.runs += 1
            
    
    def transitionTo(self,newState):
        '''
        @brief          Transitions between states
        @param newState The desired state for next iteration
        '''
        
        self.state = newState
    
    def printTrace(self):
        '''
        @brief          Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.currTime,self.startTime))
            print(str)


class Encoder:
    '''
    @brief              Class for running encoder
    '''        
    ## Initial state of encoder task FSM
    S0_init = 0
    
    ## First state of encoder FSM in which position is updated and input characters are collected and passed to user interface task
    S1_update = 1
    
    def __init__(self,taskNum,DriverObject,interval,dbg = True):
        '''
        @brief              Constructs Encoder FSM
        @param taskNum      A number to identify the task
        @param DriverObject The driver object to be referenced by this FSM object
        @param interval     An integer number of microseconds between desired iterations of the task
        @param dbg          A boolean indicating whether the task should print a trace or not
        '''
        
        
        ## Defines the starting state for the update() method
        self.state = self.S0_init
        
        ## Defines the number of the task based on constructor input
        self.taskNum = taskNum
        
        ## References driver object based on constructor input
        self.Driver = DriverObject
        
        ## Flag to print debug messages or suppress them based on constructor input
        self.dbg = dbg 
        
        ## The timestamp for the initial iteration in milliseconds
        self.startTime = utime.ticks_ms()
        
        ## Defines the interval after which another iteration will based on constructor input
        self.interval = interval
        
        ## Creates a variable to hold the index of the current iteration of the task
        self.runs = 0
        
        ## Time for which next iteration will run
        self.nextTime = utime.ticks_add(self.startTime,int(self.interval))
        
        if self.dbg:
            print('Created user interface task')
        
    def run(self):
        '''
        @brief          Runs one iteration of the encoder FSM task
        @details        Using methods defined in the Encoder and EncoderDriver class, 
                        this method iterates at a fixed interval defined by the 
                        encoder object specifications
        @image html Encoder_FSM_Diagram.png
        '''
        ## Updates to the current time recorded by the controller clock
        self.currTime = utime.ticks_ms()
        # Specifying the next time the task will run
        if utime.ticks_diff(self.currTime, self.nextTime) >= 0:
            # If the interval has been reached
            
            if(self.state == self.S0_init):
                self.printTrace()
                # Run initialization code
                self.transitionTo(self.S1_update)
                EncoderDriver.setPosition(self.Driver,0)
  
            elif(self.state == self.S1_update):
                self.printTrace()
                # Run code for state 1
                self.transitionTo(self.S1_update)
                EncoderDriver.update(self.Driver)
                if shared.cmd:
                    shared.resp = self.cipher(shared.cmd)
                    shared.cmd = None
            
            else:
                # Invalid state code (error handling)
                pass
            
            #print('Position: ' + str(EncoderDriver.getPosition(self.Driver)))
            
            self.nextTime = utime.ticks_add(self.nextTime,int(self.interval))
            
            # Increase run count
            self.runs += 1
    
            
    def cipher(self,cmd):
        '''
        @brief          Reads the shared cmd input and determines which command should be prompted, if any
        @param cmd      The shared command variable from UART
        '''
        ## if user inputs 'z'
        if cmd == 122: 
            EncoderDriver.setPosition(self.Driver,0)
            resp = print('Position: ' + str(EncoderDriver.getPosition(self.Driver)))
        ## if user inputs 'p'
        if cmd == 112: 
            resp = print('Position: ' + str(EncoderDriver.getPosition(self.Driver)))
        ## if user inputs 'd'
        if cmd == 100: 
            resp = print('Delta: ' + str(EncoderDriver.getDelta(self.Driver)))
            
        else:
            pass
    
        return resp
    
    def transitionTo(self,newState):
        '''
        @brief          Transitions between states
        @param newState The desired state for next iteration
        '''
        
        self.state = newState
            
            
    def printTrace(self):
       '''
       @brief          Prints a debug statement with a detailed trace message if the debug variable is set
       '''
       if self.dbg:
           str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.currTime,self.startTime))
           print(str)        
            
            
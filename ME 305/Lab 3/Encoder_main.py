# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 08:29:03 2020

@file               Encoder_main.py
@date               10/13/2020
@author             Miles Young
@brief              The main page for Encoder Lab 3.
@details            Source code: https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%203/Encoder_main.py
"""
import pyb
from Encoder import Encoder, EncoderDriver, UserInterface

# Create encoder pin objects
## Define pin A object using specific pyb.Pin.cpu.-- callout
pinA = pyb.Pin.cpu.A6
## Define pin B object using specific pyb.Pin.cpu.-- callout
pinB = pyb.Pin.cpu.A7
## Define the timer to be used. It must be compatible with both pins chosen for pins A and B
timer = 3

## Define pulses per cycle for encoder
PPC = 4

## Define cycles per revolution for encoder
CPR = 7

## Define gear ratio between encoder and motor
gearRatio = 50

## Defines interval which both user interface object and encoder object will use to keep time between intervals
interval = 100

## Create encoder object
DriverObject = EncoderDriver(pinA,pinB,timer,PPC,CPR,gearRatio)
## Create encoder task
EncoderObject= Encoder(1,DriverObject,interval,dbg = False)
## Create user interface task
UserObject = UserInterface(1,interval,dbg = False)

# Test code
while True:
    EncoderObject.run()
    UserObject.run()
    
    
    
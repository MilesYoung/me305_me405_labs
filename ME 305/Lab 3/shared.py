# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 12:03:55 2020

@file           shared.py
@date           10/19/2020
@author         Miles Young
@brief          Contains shared variables for Encoder task and UserInterface Task
@image html shared_Task_Diagram.png
"""

## Shared variable which holds the user command
cmd = None

## Shared variable which holds the controller response
resp = None
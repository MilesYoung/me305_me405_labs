# -*- coding: utf-8 -*-
"""
@file Fibonacci.py

@author Miles Young

@brief      A function for calculating numbers in the Fibonacci sequence.
@details    https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%201/Fibonacci.py
"""

def fibonacci(idx):
    '''
    @brief      A function for calculating numbers in the Fibonacci sequence. 
    @details    This function returns the Fibonacci number corresponding to a given
                index position (idx) in the Fibonacci sequence greater than or equal to 0. If strings,
                non-integers, or negative integers are input, the function returns
                an error requesting proper input type within the logical range.
    '''
    
    
    import numpy as np
    # Imports NumPy package later used to create array containing Fibonacci Sequence
    
    print('Calculating Fibonacci number at '
              'index n = {:}.'.format(idx))
    
    if  isinstance(idx, str) == True:
        # Accounts for string index input
        print('Please input an integer greater than or equal to 0')
    
    elif isinstance(idx, int) == False:
        # Accounts for noninteger numerical index input
        print('Please input an integer greater than or equal to 0')
    
    elif idx < 0:
        # Accounts for negative integer index input
        print('Please input an integer greater than or equal to 0')
        
    elif idx == 0:
        # Prints Fibonacci Number for index 0
        print('Fibonacci Number: 0.0')    
    
    elif idx == 1:
        # prints Fibonacci Number for index 1
        print('Fibonacci Number: 1.0')
        
    elif idx >=2:
         # Returns Fibonacci Number for any integer greater than or equal to 2
        fibseq = np.zeros(shape = idx+1)
        # Creaates array for holding values of Fibonacci Sequence by proper index        
        fibseq[1] = 1
        i = 2
        while i <= idx:
            # Populates the array for every index up to and including the desired value
            fibseq[i] = fibseq[i-1] + fibseq[i-2]
            i = i + 1 
        print('Fibonacci Number: ',fibseq[idx])
                  
    
if __name__ == '__main__': 
    ## Sample input of index position 100
    fibonacci(100)
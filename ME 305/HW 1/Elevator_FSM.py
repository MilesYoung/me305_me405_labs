# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 13:57:33 2020

@file           Elevator_FSM.py

@author         Miles Young
@date           October 3, 2020
@brief          Elevator control between two floors
@details        his code is an algorithm which controls the operation of an elevator capable of moving between two floors. 
                Link to Source Code: https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/HW%201/Elevator_FSM.py

@image html Elevator_FSM_Diagram.png

"""

from random import choice
import time

class taskElevator:
    """
    @brief          A finite state machine to control the elevator
    @details        This class implements a finite-state machine algorithm to
                    control the elevator
    """
    
    ## The initial state at which the task begins after running
    S0_Init         = 0
    
    ## Constant defining state 1
    S1_Moving_Down  = 1
    
    ## Constant defining state 2
    S2_Moving_Up    = 2
    
    ## Constant defining state 3
    S3_Floor_1      = 3
    
    ## Constant defining state 4
    S4_Floor_2      = 4
    

    
    def __init__(self,interval,taskNum,button_1,button_2,first,second,motor):
        """
        @brief              Creates a task elevator object
        @param interval     An integer number of seconds between desired iterations of the task
        @param taskNum      A number to identify the task
        @param button_1     An object from class Button representing directive to floor 1
        @param button_2     An object from class Button representing directive to floor 2
        @param first        An object from class Button representing first floor limit switch
        @param second       An object from class Button representing second floor limit switch
        @param motor        An object from class MotorDriver representing elevator winch
        """
        ## The initial state to run on each iteration of the task
        self.state = self.S0_Init
        
        ## The button object used for the go to first floor command
        self.button_1 = button_1
        
        ## The Button object used for the go to second floor command
        self.button_2 = button_2
        
        ## The button object used for the first floor limit
        self.first = first
        
        ## The button used for the second floor limit
        self.second = second
        
        ## The motor object operating the "up" and "down" transitions btwn floors
        self.motor = motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        ## The index for each elevator object
        self.taskNum = taskNum
        
    
    def run(self):
        """
        @brief      Runs one iteration of the elevator task
        """
        ## Sets the current time
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
       
            print('Elevator #' + str(self.ElevatorIndex) + ': ' + 'run ' + str(self.runs) + ', '  + 'time ' + str(round(self.curr_time-self.start_time,2)) + ' sec')        
           
            if(self.state == self.S0_Init):
                # Run State 0 code
                self.transitionTo(self.S1_Moving_Down)
                # Reset all button states
                self.button_1.ButtonReset()
                self.button_2.ButtonReset()
                self.first.ButtonReset()
                self.second.ButtonReset()
                print('State 0: Returning to floor 1')
                self.motor.Reverse()
                # Print button states
                print('Button 1: ' + str(self.button_1.state))
                print('Button 2: ' + str(self.button_2.state))
                print('First: ' + str(self.first.state))
                print('Second: ' + str(self.second.state))
                
                
            elif(self.state == self.S1_Moving_Down):
                # Run State 1 code
                print("State 1: Moving to floor 1")
                # Randomly choose floor 1 sensor state
                self.first.getButtonState()
                if(self.first.state == 1):
                    # If the sensor is on, transition to state 3
                    self.transitionTo(self.S3_Floor_1)
                    self.motor.Stop()
                    self.button_2.ButtonReset()
                else:
                    # If the sensor is not on, alert that the elevator is still in motion
                    print('*Whoosh*')
                # Print button states
                print('Button 1: ' + str(self.button_1.state))
                print('Button 2: ' + str(self.button_2.state))
                print('First: ' + str(self.first.state))
                print('Second: ' + str(self.second.state))
                
                    
            elif(self.state == self.S2_Moving_Up):
                # Run State 2 code
                print("State 2: Moving to floor 2")
                # Randomly choose floor 2 sensor state
                self.second.getButtonState()
                if(self.second.state == 1):
                    # If the sensor is on, transition to state 4
                    self.transitionTo(self.S4_Floor_2)
                    self.motor.Stop()
                    self.button_2.ButtonReset()
                else:
                    # If the sensor is not on, alert that the elevator is still in motion
                    print('*Rattle*')
                # Print button states
                print('Button 1: ' + str(self.button_1.state))
                print('Button 2: ' + str(self.button_2.state))
                print('First: ' + str(self.first.state))
                print('Second: ' + str(self.second.state))
                
            
            elif(self.state == self.S3_Floor_1):
                # Run State 3 code
                print("State 3: At floor 1")
                # Resets both buttons upon reaching floor 1
                self.button_1.ButtonReset()
                self.button_2.ButtonReset()
                # Randomly determines which buttons are pressed
                self.button_1.getButtonState()
                self.button_2.getButtonState()
                if(self.button_2.state == 1):
                    # Transitions to state 2 if button 2 is pushed
                    self.transitionTo(self.S2_Moving_Up)
                    print("Floor 2 button pushed")
                    self.motor.Forward()
                    # Resets the floor 1 sensor
                    self.first.ButtonReset()
                    # Resets button 1 in case it was also randomly pressed
                    self.button_1.ButtonReset()
                elif(self.button_1.state == 1):
                    # If button 2 is not pushed but button 1 is, remain on floor 1
                    print('Floor 1 button pushed, already at floor 1')
                else:
                    # If neither button is pressed
                    print('No button pushed, elevator will remain on floor 1')
                # Print button states
                print('Button 1: ' + str(self.button_1.state))
                print('Button 2: ' + str(self.button_2.state))
                print('First: ' + str(self.first.state))
                print('Second: ' + str(self.second.state))
                    
            elif(self.state == self.S4_Floor_2):
                # Run State 4 code
                print("State 4: At floor 2")
                # Resets both buttons upon reaching floor 2
                self.button_1.ButtonReset()
                self.button_2.ButtonReset()
                # Randomly determines which buttons are pressed
                self.button_1.getButtonState()
                self.button_2.getButtonState()
                if(self.button_1.state == 1):
                    # Transitions to state 1 if button 1 is pushed
                    self.transitionTo(self.S1_Moving_Down)
                    print("Floor 1 button pushed")
                    self.motor.Reverse()
                    # Resets the floor 2 sensor
                    self.second.ButtonReset()
                    # Resets button 2 in case it was also randomly pressed
                    self.button_2.ButtonReset()
                elif(self.button_2.state == 1):
                    # If button 1 is not pushed but button 2 is, remain on floor 2
                    print('Floor 2 button pushed, already at floor 2')
                else:
                    # If neither button is pressed
                    print('No button pushed, elevator will remain on floor 2')
                # Print button states
                print('Button 1: ' + str(self.button_1.state))
                print('Button 2: ' + str(self.button_2.state))
                print('First: ' + str(self.first.state))
                print('Second: ' + str(self.second.state))
            
            else:
                # Invalid State code (Error Handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval 
            
            # Put a dividing line between each run's output
            print('')

    def transitionTo(self,newState):
        """
        @brief      Updates the variable defining the next state to run
        """
        self.state = newState
                
                
class Button:
    
    """
    @brief          A pushbutton class
    @details        This class represents a button that the can be pushed by the
                    imaginary elevator operator. As of right now
                    this class is implemented using "pseudo-hardware". That is, we
                    are not working with real hardware IO yet, this is all pretend 
    """
   
    ## Constant defining ON state
    ON         = 1
    
    ## Constant defining OFF state
    OFF        = 0
    
    def __init__(self,pin):
        """
        @brief          Creates a button object
        @param pin     An object designating pin location for button inputs
        """
        ## Defines which physical pin the button object is attached to
        self.pin = pin
    
        print('Button object created attached to pin ' + str(self.pin))
    
    def ButtonReset(self):
        '''
        @brief          Resets the value of a button
        @details        Used to reset a button value from 1 to 0
        '''
        
        ## Resets button state to OFF
        self.state = self.OFF
    
    
    def getButtonState(self):
        '''
        @brief          Gets the button state.
        @details        Since there is no hardwared attached this method returns a randomized true or false value
        @return         Either ON = 1 or OFF = 0 representing the state of the button object
        '''
        self.state = choice([self.ON, self.OFF])
    
    
class motorDriver:
    '''
    @brief          A class representing the motor used to facilitate the movement of the elevator
    @details        The motor objects in this class have three possible states:
                    forward motion, reverse motion, or no motion (stationary)
    '''
    
    ## Constant defining forward motion
    FWD = 1
    
    ## Constant defining reverse motion
    RVS = 2
    
    ## Constant defining no motion (stopped)
    STP = 0
    
    def __init__(self):
        '''
        @brief      Constructs the motor object
        '''
        pass
    
    def Forward(self):
        '''
        @brief      Sets the motor object state to forward and prints state value
        '''
        ## Sets motor state to forward
        self.state = self.FWD
        print("Motor Moving 'Upwards,'" + ' motor: ' + str(self.state) )
        
    def Reverse(self):
        '''
        @brief      Sets the motor object state to reverse and prints state value
        '''
        
        ## Sets motor state to reverse
        self.state = self.RVS
        print("Motor moving 'downwards,'" + ' motor: ' + str(self.state))
        
    def Stop(self):
        '''
        @brief      Sets the motor object state to stop and prints state value
        '''
        ## Sets motor state to stopped
        self.state = self.STP
        print("Motor stopped," + ' motor: ' + str(self.state))

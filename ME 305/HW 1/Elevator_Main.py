# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 16:17:49 2020
@file           Elevator_Main.py
@author         Miles Young
@date           10/06/2020
@brief          Main file into which elevator FSMs are imported and run


"""

from Elevator_FSM import Button, motorDriver, taskElevator

# Creating Objects to pass into task constructor
## First floor button object
button_1        = Button('PB6')
## Second floor button object
button_2        = Button('PB7')
## First floor sensor object
first           = Button('PB8')
## Second floor sensor object
second          = Button('PB9')
## Motor object
motor           = motorDriver()

## First elevator task object
Elevator_1 = taskElevator(0.1,1,button_1,button_2,first,second,motor)
## Second task elevator object
Elevator_2 = taskElevator(0.1,2,button_1,button_2,first,second,motor)


# Create new line to differentiate when task outputs begin
print('')

# Run two elevator tasks simultaneously
for N in range(5000000):
    Elevator_1.run()
    Elevator_2.run()

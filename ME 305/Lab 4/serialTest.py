# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 08:57:00 2020

@author: Miles
"""

import serial

ser = serial.Serial(port = 'COM3',baudrate = 115273,timeout = 1)


def sendChar():
    inv = input('Give me a character: ')
    ser.write(str(inv).encode('ascii'))
    myval = ser.readline().decode('ascii')
    print(myval)

for n in range(3):
    sendChar()

ser.close()
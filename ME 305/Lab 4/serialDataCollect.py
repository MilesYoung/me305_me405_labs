# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 09:34:27 2020

@file           serialDataCollect.py
@author         Miles Young
@date           10/20/2020
@brief          Sensor data collection.\n
@details        This code utilizes a continuously running FSM to collect 
                encoder position and corresponding time data according to 
                serial commands from the PC. It then sends the data as a string
                to the PC. 
                Sourcecode: https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%204/serialDataCollect.py 
"""
from pyb import UART
import uarray
import utime
import math
from Encoder import EncoderDriver

class DataCollect:
    '''
    @brief      Encoder data collection class
    @details    This class consists of a constructor, run() method, and
                transitionTo(newState) method. The run() method is a FSM which
                has an initialization state, a state which waits for the command
                to begin data collection and also prepares empty arrays and 
                time limits, and a data collection state which also waits for 
                command to terminate data collection early.
    '''
    
    ## Initial state of data collection task FSM
    S0_init = 0
    
    ## First state of data collection task FSM which waits for character input from user through UART
    S1_begincollect = 1
    
    ## Second state of data collection task FSM which passes character input to user interface front end through UART
    S2_endcollect = 2

    def __init__(self,taskNum,DriverObject,runtime):
        '''
        @brief              Construct the user interface
        @param taskNum      A number to identify the task
        @param DriverObject The encoder object from which data will be collected
        @param runtime      The desired data collection runtime
        '''
        
        ## Defines the starting state for the run() method
        self.state = self.S0_init
        
        ## Defines the number of the task based on constructor input
        self.taskNum = taskNum
        
        ## References driver object based on constructor input
        self.Driver = DriverObject
        
        ## Defines how long data collection will occur according to user input
        self.runtime = runtime
        
        ## Defines variable to hold command from REPL
        self.cmd = None
        
        ## Defines a variable to hold response from controller
        self.resp = None
        
        ## The timestamp for the initial iteration in milliseconds
        self.startTime = utime.ticks_ms()
        
        ## Defines the current time for the iteration and is overwritten at the beginning of each iteration
        self.currTime = utime.ticks_ms()
        
        ## Defines the interval after which another iteration will run as (pulses/PPS)
        self.interval = 100
        
        ## Time for which next iteration will run and is overwritten at the end of each iteration
        self.nextTime = utime.ticks_add(self.startTime,self.interval)
        
        ## Holds the time at which data collection must end in milliseconds + the time interval between when the variable is defined in state 1 and when data collection will begin in state 2. Note that in order to avoid running too long the current time is rounded down to the nearest 100th place
        self.finTime = utime.ticks_add(utime.ticks_add(100*math.floor(self.currTime/100),self.runtime),self.interval)
        
        ## Creates an empty array to hold time
        self.time = uarray.array('i')
        
        ## Creates an empty array to hold data
        self.position  = uarray.array('i')
        
        ## Creates a variable to hold the index of the current iteration of the task
        self.runs = 0
        
        ## Defines the serial port for communication with the controller
        self.myuart = UART(2)
        
        
    
    def run(self):
        '''
        @brief          Runs one iteration of the data collection task
        @image html serialDataCollect_FSM_Diagram.png
        '''
    
        ## Updates to the current time recorded by the controller clock
        self.currTime = utime.ticks_ms()
        
        # Specifying the next time the task will run
        if utime.ticks_diff(self.currTime, self.nextTime) >= 0:
            # If the interval has been reached
    
            # SO opens serial port
            if(self.state == self.S0_init):
                # Run state 0 code
                
                # Clear arrays to hold data
                self.time = uarray.array('i')
                self.position = uarray.array('i')
                # Transition to next state
                self.transitionTo(self.S1_begincollect)
            
    
            elif(self.state == self.S1_begincollect):
                # Run state 1 code
                
                if self.myuart.any() != 0:
                    if(self.myuart.readchar() == 103):
                        # Read command
                        self.cmd = self.myuart.readchar()
                        # Clear command
                        self.cmd = None
                        # Send response
                        self.resp = 'Beginning data collection'
                        print(self.resp)
                        # Clear response
                        self.resp = None
                        # Set up the tools for data collection in next state 
                        # Define the time at which data collection will automatically terminate. This is the current time + 10 seconds + the time interval between the current time and when the data collection will begin
                        self.finTime = utime.ticks_add(100*math.ceil(self.currTime/100),self.runtime) + self.interval
                        # Transition to next state
                        self.transitionTo(self.S2_endcollect)
                    else:
                        # Error handling
                        pass
                else:
                    # Remain in current state until command is recieved
                    pass
                    
            elif(self.state == self.S2_endcollect):
                # Run state 2 code
                
                # If there is a command waiting in the bus
                if self.myuart.any() != 0:
                    # Checks for character in bus
                    self.cmd = self.myuart.readchar()
                    if(self.cmd == 115):
                        # If immediate termination is requested:
                        # Clear command
                        self.cmd = None
                        # Return to previous state
                        self.transitionTo(self.S0_init)
                        # Notify user that data collection has been terminated early
                        self.resp = 'Terminating data collection early'
                        print(self.resp)
                        # Clear response
                        self.resp = None
                        # Send data collection to the PC
                        for k in range(len(self.position)):
                            print('{:},{:}'.format(self.time[k],self.position[k]))
                    else:
                        # If incorrect command is sent, update should still occur
                        self.cmd = None
                        # Update encoder driver position
                        self.time.append(self.interval*len(self.time))
                        EncoderDriver.update(self.Driver)
                        # Add updated position to end of data array
                        self.position.append(EncoderDriver.getPosition(self.Driver))
                else:
                    # This part of the code is where the actual data collection occurs
                    # Remain in current state until command is received or data collection is automatically terminated
                    self.time.append(self.interval*len(self.time))
                    if utime.ticks_diff(self.currTime,self.finTime) <0:
                        # If runtime has not been reached:
                        # Update encoder driver position
                        EncoderDriver.update(self.Driver)
                        # Add updated position to end of data array
                        self.position.append(EncoderDriver.getPosition(self.Driver))
                    elif utime.ticks_diff(self.currTime,self.finTime) >= 0:
                        # If runtime has been reached:
                        # Return to previous state
                        self.transitionTo(self.S0_init)
                        # Notify user that data collection is complete
                        self.resp = 'Data collection complete'
                        print(self.resp)
                        # Clear response
                        self.resp = None
                        # Send data collection to PC
                        for k in range(len(self.position)):
                            print('{:},{:}'.format(self.time[k],self.position[k]))
                
            # Define time after which the data collection task will commence
            self.nextTime = utime.ticks_add(self.nextTime,int(self.interval))
                
            # Increase run count by 1
            self.runs += 1
    
    def transitionTo(self,newState):
        '''
        @brief          Transitions between states
        @param newState The desired state for next iteration
        '''
        
        self.state = newState
    

# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 08:23:40 2020

@file               serial_main.py
@date               10/127/2020
@author             Miles Young
@brief              The main page for serial data collection Lab 4.\n
@details            This code references the EncoderDriver and DataCollect 
                    classes. Encoder position data collection begins when
                    prompted through serialFrontUI file and continues for the 
                    runtime duration determined by user through DataCollect 
                    object constructor. This code runs continuously regardless
                    of whether or not inputs are given. 
                    Sourcecode: https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%204/serial_main.py
@image html serial_Task_Diagram.png
"""

import pyb
from Encoder import EncoderDriver
from serialDataCollect import DataCollect

# Create encoder pin objects
## Define pin A object using specific pyb.Pin.cpu.-- callout
pinA = pyb.Pin.cpu.A6
## Define pin B object using specific pyb.Pin.cpu.-- callout
pinB = pyb.Pin.cpu.A7
## Define the timer to be used. It must be compatible with both pins chosen for pins A and B
timer = 3


## Create encoder object with pinA, pinB, and timer in the constructor
DriverObject = EncoderDriver(pinA,pinB,timer)
## Define the desired runtime, which is then input into DataObject constructor
runtime = 15000 # units: ms
## Create encoder task with object identifier 1, DriverObject, and runtime in the constructor
DataObject= DataCollect(1,DriverObject,runtime)

# run code continuously
while True:
    DataObject.run()
    
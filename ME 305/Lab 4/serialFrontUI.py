# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 09:32:32 2020

@file           serialFrontUI.py
@author         Miles Young
@date           10/20/2020
@brief          Front-end User Interface.\n
@details        This code utilizes a finite state machine to send commands to
                a controller, either initiating or terminating data collection
                from a sensor, and recieve susequent responses. It saves the 
                collected sensor data as a .csv file and plots it as well. 
                Sourcecode: https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%204/serialFrontUI.py
"""

import serial
import array
import time
import keyboard
import numpy as np
import matplotlib.pyplot as plt

class FrontUI:
    '''
    @brief              User interface front end class
    @details            This class allows a user to interact with a sensor
                        connected to the controller through serial 
                        communication. It can prompt the controller to begin
                        collecting data using a 'g' keystroke and stop the data
                        collection using 's' keystroke. It accepts an array of 
                        data from the controller, which it then plots as well as 
                        converts to a .csv (comma-seperated values) file 
    '''     
    ## Initial state of user interface front end task FSM
    S0_init = 0
    
    ## First state of user interface front end task FSM which waits for character input through UART
    S1_begin = 1
    
    ## Second state of user interface front end task FSM which passes character input to controller through UART
    S2_end = 2
    
    ## Final state of user interface front end task FSM which closes the serial port
    S3_close = 3
    
    def __init__(self,taskNum,dbg = True):
        '''
        @brief          Construct the user interface
        @param taskNum  A number to identify the task
        @param dbg      A boolean indicating whether the task should print a trace or not
        '''
        
        ## Defines the starting state for the run() method
        self.state = self.S0_init
        
        ## Defines the previous state, used for conditional statement in State 0 initialization
        self.prevstate = self.S0_init
        
        ## Defines the number of the task based on constructor input
        self.taskNum = taskNum
          
        ## Flag to print debug messages or suppress them based on constructor input
        self.dbg = dbg  
        
        ## Creates a variable to hold the index of the current iteration of the task
        self.runs = 0
        
        ## Defines the serial port for communication with the controller
        self.ser = serial.Serial(port = 'COM3',baudrate = 115273,timeout = 1)
        
        ## Defines variable to hold command from REPL
        self.cmd = None
        
        ## Defines a variable to hold response from controller
        self.resp = None
        
        ## Defines a list to hold the unformatted string of data recieved from controller
        self.readData = []
        
        ## Defines list to directly read from controller
        self.time = array.array('i')
        
        ## Defines list to hold properly converted and formatted data
        self.position = array.array('i')
        
        ## Defines an indicator of whether the FSM should continue to iterate
        self.iterate = True
        
        if self.dbg:
            print('Initiated user interface front end')
        
    def run(self):
        '''
        @brief          Runs one iteration of the user interface front end task
        @image html serialFrontUI_FSM_Diagram.png
        '''
         
        # SO opens serial port
        if(self.state == self.S0_init):
            # Run state 0 code
            
            # Debugging
            self.printTrace()
            
            if(self.prevstate == self.S0_init):
                # Clear any pre-existing characters
                self.resp = self.ser.readline().decode('ascii') 
                # Clear response
                self.resp = None
                print("Press 'g' to begin data collection")
                self.transitionTo(self.S1_begin)
            elif(self.prevstate == self.S1_begin):
                print("Press 's' to terminate data collection early")
                self.transitionTo(self.S2_end)
                    
        elif(self.state == self.S1_begin):
            # Run state 1 code 
            
            # Debugging
            self.printTrace()
            # Prompt user input
            if(keyboard.is_pressed('g')):
                
                # Send command
                self.ser.write(str('g').encode('ascii'))
                # Clear command
                self.cmd = None
                # Read response
                self.resp = self.ser.readline().decode('ascii')
                print(self.resp)
                # Clear response
                self.resp = None
                # Transition to next state
                self.transitionTo(self.S0_init)
                # Redefine previous state
                self.prevstate = self.S1_begin
            else:
                # Error handling
                pass
        
            
        elif(self.state == self.S2_end):
            # Run state 2 code
            
            # Debugging
            self.printTrace()
            # Prompt user input
            
            print('Collecting...')
            
            if(self.ser.in_waiting !=0):
                 # Read response
                self.resp = self.ser.readline().decode('ascii')
                print(self.resp)
                # Clear response
                self.resp = None
                # Read, reformat, save, and plot data
                self.processData()
                # Transition to closing state
                self.transitionTo(self.S3_close)
                
            elif(keyboard.is_pressed('s')):
                # Send user input
                self.ser.write(str('s').encode('ascii'))
                # Clear command
                self.cmd = None
                # Read response
                self.resp = self.ser.readline().decode('ascii')
                print(self.resp)
                # Clear response
                self.resp = None
                # Read, reformat, save, and plot data
                self.processData()
                # Transition to closing state
                self.transitionTo(self.S3_close)
            else:
                # Error handling
                pass
        
        elif(self.state == self.S3_close):
            # Close serial port communication
            self.ser.close()
            self.iterate = False
            
        self.runs += 1
        
        
    
    def transitionTo(self,newState):
        '''
        @brief          Transitions between states
        @param newState The desired state for next iteration
        '''
        
        self.state = newState
    
    def printTrace(self):
        '''
        @brief          Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}'.format(self.taskNum, self.state, self.runs)
            print(str)
    
    def processData(self):
        '''
        @brief          This method is responsible for reading and interpreting the data sent in string format from the encoder. It strips return and newline statements from strings, splits according to the comma between time and position values, saves the newly combed data as a .csv file, and populates a time and position array which are then plotted.
        '''
        
        #Delay 0.5 seconds to allow transfer of data
        time.sleep(0.5)
        # Read data array
        while self.ser.in_waiting > 0:
            self.readData.append(self.ser.readline().decode('ascii').strip('\r\n').split(','))
        # Save data array to .csv file
        np.savetxt('Encoder Position vs. Time.csv',self.readData,delimiter=',',fmt='%s',header='Time [ms], Encoder Position [-]')
        print('.csv file saved')
        for n in range(len(self.readData)):
            self.time.append(int(self.readData[n][0]))
            self.position.append(int(self.readData[n][1]))
        # Plot data array
        plt.plot(self.time,self.position)
        plt.title('Encoder Position vs. Time')
        plt.ylabel('Encoder Position [-]')
        plt.xlabel('Time [ms]')
        plt.show()    

        
        
        
        
# Run User Interface Front End

## Create user interface task
UserObject = FrontUI(1,dbg = False)

while UserObject.iterate == True:
    UserObject.run()

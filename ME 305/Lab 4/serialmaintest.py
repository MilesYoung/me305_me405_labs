# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 17:13:48 2020

@author: Miles
"""
from pyb import UART
myuart = UART(2)

while True:
    if myuart.any() !=0:
        val = myuart.readchar()
        print('You sent an ASCII ' + str(val) + ' to the Nucleo')
        
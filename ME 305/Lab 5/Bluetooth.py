# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 09:43:19 2020

@file           Bluetooth.py
@author         Miles Young
@date           11/8/2020
@brief          A driver for bluetooth communication with a microcontroller.\n
@details        This code contains a Bluetooth class which is primarily used to 
                define a bluetooth object that can be controlled using the BT_task
                task. The bluetooth UART and baudrate are assigned by the user 
                upon construction. The subsequent methods in this class allow 
                for checking for characters in the UART bus, reading character 
                commands from the bluetooth controller, writing to the controller, 
                turning the LED on, or turning the LED off.\n
                Sourcecode: https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%205/Bluetooth.py
"""

import pyb


class Bluetooth:
    '''
    @brief          A driver class for bluetooth communication
    '''

    def __init__(self,objNum,UARTNum,baudrate):
        '''
        @brief          Constructs a bluetooth object
        @param objNum  A number to identify the object
        @param UARTNum  The number designator of the UART used for bluetooth serial communication
        @param baudrate The required baudrate for the bluetooth UART serial communication
        '''
        
        ## Defines the number of the task based on constructor input
        self.objNum = objNum
        
        ## Defines the serial port for communication with the controller
        self.myuart = pyb.UART(UARTNum,baudrate)
        
        ## Set up LED pin on MCU
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
        
    def checkBT(self):
        '''
        @brief          Checks for any characters waiting in the serial bus
        '''
        if self.myuart.any() != 0:
            return True
        else:
            return False
    

    def readBT(self):
        '''
        @brief          Reads characters one at a time and returns their ASCII number values added together
        '''
        
        cmd = 0

        while self.myuart.any() != 0:
            cmd = cmd + self.myuart.readchar()
        
        return cmd
        
    def writeBT(self,string):
        '''
        @brief          Prints a string input to the controller via bluetooth serial communication
        '''
        self.myuart.write(str(string) + '\r\n')
        
        
    def BTon(self):
        '''
        @brief          Turns the LED on
        '''
        self.pinA5.high()
        
        
    def BToff(self):
        '''
        @brief          Turns the LED off
        '''
        self.pinA5.low()
        
        
        

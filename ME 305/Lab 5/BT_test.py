# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 09:07:17 2020

@author: Miles
"""

import pyb
from pyb import UART

## Initializes UART serial communication for given Bluetooth pins 
uart = UART(3,9600)

## Set up LED pin on MCU
pinA5 = pyb.Pin(pyb.Pin.cpu.A5)

## Code to recieve Bluetooth commands
while True:
    if uart.any() != 0:
        val = int(uart.readline())
        if val == 0:
            print(val,'Turns it OFF')
            pinA5.low()
        elif val == 1:
            print(val,'Turns it ON')
            pinA5.high()
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 09:43:10 2020

@file           BT_main.py
@author         Miles Young
@date           11/8/2020
@brief          Main file for the bluetooth LED blinking function.\n
@details        This file defines a bluetooth object using the Bluetooth driver class which is then
                used to define a BluetoothBlink task class object. This task is then run indefinitely on the Nucleo.\n
                Sourcecode: https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%205/BT_main.py
@image html Bluetooth_Task_Diagram.png
"""

from Bluetooth import Bluetooth
from BT_task import bluetoothBlink

## Define bluetooth object
BlueObject = Bluetooth(1,3,9600)

## Define bluetooth task
BlueTask = bluetoothBlink(1,BlueObject,dbg = True)

## Run bluetooth task indefinitely
while True:
    BlueTask.run()
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 09:43:52 2020

@file           BT_task.py
@author         Miles Young
@date           11/8/2020
@brief          Bluetooth-controlled blinking LED task.\n
@details        This task allows for the frequency at which a built-in LED on 
                the Nucleo board blinks to be controlled by a user through bluetooth 
                serial communication. It references methods in the Bluetooth class
                and requires a Bluetooth object to interact with.\n
                Sourcecode: https://bitbucket.org/MilesYoung/me305_me405_labs/src/master/Lab%205/BT_task.py
"""

import utime
from Bluetooth import Bluetooth

class bluetoothBlink:
    '''
    @brief      Class for LED blinking task
    @image html Bluetooth_FSM_Diagram.png
    '''
    
    ## The initial state at which the task begins upon running
    S0_init = 0
    
    ## A state in which code does nothing, essentially a full "power off"
    S1_doNothing = 1
    
    ## A state for checking and handling commands sent through UART 
    S2_checkChar = 2
    
    ## A state for handling LED blinking function
    S3_blink = 3
    
    ## LED state for power on
    ON = 4
    
    ## LED state for power off
    OFF = 5
    
    def __init__(self,taskNum,BlueObject,dbg = True):
        '''
        @brief                  Constructs the bluetooth-controlled LED blinking task
        @param taskNum          A number to identify the task
        @param BlueObject       The bluetooth object from which data will be collected and to which message updates will be sent
        @param dbg              A boolean indicating whether the task should print a trace or not. The trace will be printed to the bluetooth controlling device.
        '''
        
        ## Defines the initial state for the task
        self.state = self.S0_init
        
        ## Defines previous state after transition to S2 so that current function can be continued in the case of non-meaningful input
        self.prevState = self.S0_init
        
        ## Defines the initial state for the LED (ON or OFF)
        self.LEDstate = self.ON
        
        ## Defines the number of the task based on constructor input
        self.taskNum = taskNum
        
        ## References driver object based on constructor input
        self.BlueObject = BlueObject
        
        ## Flag to print debug messages or suppress them based on constructor input
        self.dbg = dbg 
        
        ## Input that defines the blink frequency. Because the user bluetooth input is in Hz and the Nucleo keeps time in ms, the value is converted to kHz.
        self.frequency = 1*1000
        
        ## Convert input frequency to period in ms for timekeeping purposes
        self.period = 1/self.frequency
        
        ## Defines incremental step as 1000 ms
        self.increment = 100
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the initial iteration in milliseconds
        self.startTime = utime.ticks_ms()
        
        ## Defines the current time for the iteration and is overwritten at the beginning of each iteration
        self.currTime = utime.ticks_ms()
        
        ## Time for which next iteration will run
        self.nextTime = utime.ticks_add(self.startTime,self.increment)
        
        ## Time for which each period of blink begins
        self.blinkstartTime = utime.ticks_ms()
        
        ## Defines variable to hold command from bluetooth controller
        self.cmd = None
        
        ## Defines a variable to hold response to be sent to bluetooth controller
        self.resp = None
        
        if self.dbg:
            print('Initiated bluetooth LED blinking task')
        
    def run(self):
        '''
        @brief          Runs one iteration of the LED blinking task
        @details        This task runs a new iteration every 100 ms. It is structured 
                        as an FSM which includes an initial state, a standby state, 
                        a command-interpreting state, and a blinking state.
                        
        '''
        
        # Updates to the current time recorded by the controller clock
        self.currTime = utime.ticks_ms()
        if utime.ticks_diff(self.currTime,self.nextTime) >= 0:
        
            # Debugging
            self.printTrace()
            
            # Run state 0 initialization
            if(self.state == self.S0_init):
                Bluetooth.BToff(self.BlueObject)
                self.LEDstate = self.OFF
                self.resp = 'Waiting for user input: Please input a value between 1 and 10'
                Bluetooth.writeBT(self.BlueObject,self.resp)
                self.resp = None
                self.transitionTo(self.S1_doNothing)
            
            # Run state 1 do nothing
            elif(self.state == self.S1_doNothing):
                if Bluetooth.checkBT(self.BlueObject):
                    self.prevState = self.state
                    self.transitionTo(self.S2_checkChar)
                else:
                    pass
            
            # Run state 1 checking for and handling user input
            elif(self.state == self.S2_checkChar):                
                # Read bluetooth serial commands. The FSM only enters this state
                #if another state detects a character in the bus, so it is assumed that a character is waiting
                self.cmd = Bluetooth.readBT(self.BlueObject)
                if self.cmd >= 49 and self.cmd <= 57:
                    self.frequency = (self.cmd-48)/1000
                    self.period = 1/self.frequency
                    self.resp = 'Frequency set to ' + str(self.cmd-48) + 'Hz'
                    Bluetooth.writeBT(self.BlueObject,self.resp)
                    self.resp = None
                    self.transitionTo(self.S3_blink)
                    self.cmd = None
                elif self.cmd == (49 + 48): 
                    self.frequency = (self.cmd-87)/1000
                    self.period = 1/self.frequency
                    self.resp = 'Frequency set to ' + str(self.cmd-87) + 'Hz'
                    Bluetooth.writeBT(self.BlueObject,self.resp)
                    self.resp = None
                    self.transitionTo(self.S3_blink)
                    self.cmd = None
                elif self.cmd == 48:
                    self.transitionTo(self.S1_doNothing)
                    Bluetooth.BToff(self.BlueObject)
                    self.LEDstate = self.OFF
                    self.resp = 'LED turned off'
                    Bluetooth.writeBT(self.BlueObject, self.resp)
                    self.resp = None
                    self.cmd = None
                else:
                    self.resp = 'Frequency input is out of bounds: Please input a value between 1 and 10 to define frequency, or 0 to power off'
                    Bluetooth.writeBT(self.BlueObject,self.resp)
                    self.resp = None
                    if self.prevState == self.S3_blink:
                        self.transitionTo(self.S3_blink)
                    elif self.prevState == self.S1_doNothing:
                        self.transitionTo(self.S1_doNothing)
                    self.cmd = None
                    
                
    
            # Run state 2 handling LED blinking    
            elif(self.state == self.S3_blink):
                #Check for user input
                if Bluetooth.checkBT(self.BlueObject):
                    self.prevState = self.state
                    self.transitionTo(self.S2_checkChar)
                # If no user input, commence regular blinking according to previously defined interval
                else:
                    if utime.ticks_diff(self.currTime,self.blinkstartTime) >= self.period:
                        if self.LEDstate == self.ON:
                            Bluetooth.BToff(self.BlueObject)
                            self.blinkstartTime = self.currTime
                            self.LEDstate = self.OFF
                        elif self.LEDstate == self.OFF:
                            Bluetooth.BTon(self.BlueObject)
                            self.blinkstartTime = self.currTime
                            self.LEDstate = self.ON
                    else:
                        pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.nextTime = utime.ticks_add(self.nextTime,int(self.increment))
            
    
        
    def transitionTo(self,newState):
        '''
        @brief          Transitions between states
        @param newState The desired state for next iteration
        '''
        
        self.state = newState
    
    def printTrace(self):
        '''
        @brief          Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/O{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.BlueObject.objNum, self.state, self.runs, utime.ticks_diff(self.currTime,self.startTime))
            print(self.BlueObject,str)   
        
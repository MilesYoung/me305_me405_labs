'''
@file TaskCipher.py
@brief Character cipher waiting for intermittent use by the user
@author Charlie Refvem
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import shares
import utime

class TaskCipher:
    '''
    Cipher task.
    
    An object of this class encodes characters sent by a TaskUser object. The chosen cipher is to change the case fo the letter a->A and A->a but leave all other characters unchanged. Caps-lock in essence.
    
    @image html fsm_task_02.png width=1200px
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Wait for command state
    S1_WAIT_FOR_CMD     = 1

    def __init__(self, taskNum, interval, dbg=True):
        '''
        Creates a cipher task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The name of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        if self.dbg:
            print('Created scaler task')

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S1_WAIT_FOR_CMD):
                self.printTrace()
                # Run State 1 Code
                if shares.cmd:
                    shares.resp = self.cipher(shares.cmd)
                    shares.cmd = None
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
    
    def cipher(self, cmd):
        '''
        Changes case; A->a and a->A; for all letters a-z
        '''
        if cmd >= 65 and cmd <= 90:
            resp = cmd + 32
        elif cmd >= 97 and cmd <= 122:
            resp = cmd - 32
        else:
            resp = cmd
            
        return resp
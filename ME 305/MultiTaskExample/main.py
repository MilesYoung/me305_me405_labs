'''
@file main.py
@brief Main program runs all the tasks round-robin
@author Charlie Refvem
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import shares
from TaskUser import TaskUser
from TaskCipher import TaskCipher

## User interface task, works with serial port
task0 = TaskUser  (0, 1_000, dbg=False)

## Cipher task encodes characters sent from user task by flipping the case of the character if it is a letter
task1 = TaskCipher(1, 1_000, dbg=False)

## The task list contains the tasks to be run "round-robin" style
taskList = [task0,
            task1]

while True:
    for task in taskList:
        task.run()
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 08:23:41 2021

@file       Vendotron.py
@author     Miles Young
@date       01/14/2021
@image html Vendotron_FSMDiagram.png
@brief      <b> Vending machine FSM task </b> \n
@details    This vending machine finite state machine is capable of accepting 
            payments in denominations from pennies to twenty dollar bills. It 
            dispenses four different brands of soda. Prices and directions for 
            paying and selecting a beverage are printed in the initial state of 
            the task. In the next state payments are accepted and added to a 
            running balance and a beverage selection can be made. If the balance 
            is sufficient, a drink will be dispensed in the next state. If it 
            is not, then a printed message will notify the user. In both cases, 
            the FSM returns to the previous state and awaits another payment/drink 
            selection. If the user requests the balance be returned, the FSM 
            transitions to a final state in which this function is completed, 
            after which the FSM returns to the initial state.
"""

import keyboard
import math
import random

def on_keypress (thing):
    '''
    @brief      Callback which runs when the user presses a key.
    '''
    global pushed_key

    pushed_key = thing.name


def sumBalance(balance):
    '''
    @brief      Sums the current payment balance in dollars
    '''
    
    return float(balance[0] + balance[1]*5 + 
            balance[2]*10 + balance[3]*25 + balance[4]*100 + 
            balance[5]*500 + balance[6]*1000 + balance[7]*2000)/100
        

def getChange(price, payment):
    '''
    @brief          A function that calculates correct change for a given purchase.
    @details        This function returns the change owed for a given purchase 
                    in the fewest denominations possible. The function accepts 
                    two inputs: the price of the purchase as a float, and the 
                    amount payed as a tuple holding the integer number of denominations. 
                    Note: if the sum of the payment exceeds 10 trillion, 
                    resolution will decrease, beginning with rounding to nearest 
                    tenth of a cent followed by rounding to the nearest dollar at 10 quadrillion.
    @param price    A floating point value representing the price of the purchase
                    in dollars and cents. If the resolution of the given price 
                    is greater than 1 cent, the price will be rounded to the nearest cent.
    @param payment  A tuple holding the integer number of each denomination used
                    in the payment, from pennies to twenty dollar bills.
    @return         If funds are sufficient, returns a tuple of change, if unsufficient or 
                    other typos detected, returns None.
    '''
    
    # Rejects non-integer denomination quantities
    payflag = False
    for k in range(len(payment)):
        if type(payment[k]) != int:
            print('Denomination quantity ' + str(payment[k]) + ' (index ' + str(k) + ')' + ' must be an integer value')
            payflag = True
        elif payment[k] < 0:
            print('Denomination quantity ' + str(payment[k]) + ' (index ' + str(k) + ')' + ' must be a non-negative value')
            payflag = True
        else:
            pass    
    if payflag == True:
        return None
    
    # Rejects negative price values
    priceflag = False
    if price < 0:
        priceflag = True
    else:
        pass
    if priceflag == True:
        print('Please input a non-negative price value')
        return None
    
    # Rounds price to nearest cent if necessary
    price = round(price*100)/100
    
    # Sums the denominations in payment tuple
    pay = int(payment[0] + payment[1]*5 + 
           payment [2]*10 + payment[3]*25 + payment[4]*100 + 
           (payment[5]*500) + (payment[6]*1000) + (payment[7]*2000))
    print('Sum of balance: ' + '{:.2f}'.format(pay/100) + ' dollars')
    
    # Check to see if payment is sufficient
    if pay >= price*100:
        # Calculate the sum of change owed
        tally = int(pay - price*100)
        print('Sum of change owed: ' + '{:.2f}'.format(tally/100) + ' dollars')
        
        # Calculate the number of each denomination required from largest to smallest and 
        # subtracting their value from the tally,thereby ensuring the least number 
        # of denominations is returned
        NumXXs = math.floor(tally/2000)
        tally = tally - NumXXs*2000
        #print(str(tally) +' cents left')
        
        NumXs = math.floor(tally/1000)
        tally = tally - NumXs*1000
        #print(str(tally) +' cents left')
        
        NumVs = math.floor(tally/500)
        tally = tally - NumVs*500
        #print(str(tally) +' cents left')
        
        NumIs = math.floor(tally/100)
        tally = tally - NumIs*100
        #print(str(tally) +' cents left')
        
        Numxxvs = math.floor(tally/25)
        tally = tally - Numxxvs*25
        #print(str(tally) +' cents left')
        
        Numxs = math.floor(tally/10)
        tally = tally - Numxs*10
        #print(str(tally) +' cents left')
        
        Numvs = math.floor(tally/5)
        tally = tally - Numvs*5
        #print(str(tally) +' cents left')
        
        Numis = math.floor(tally)
        tally = tally-Numis
        #print(str(tally) +' cents left')
         
        # Populating the change tuple with numbers of denominations
        change = [Numis,Numvs,Numxs,Numxxvs,NumIs,NumVs,NumXs,NumXXs]
        return change
    
    # Notifies user that payment is not sufficient
    else:
        print('Insufficient Funds >:(')
        return payment
    
       
def printWelcome(priceC,priceP,priceS,priceD):
    '''
    @brief          Startup welcome message
    @details        This function is called in the initialization state of the Vendotron FSM. 
                    It displays the predefined prices for each beverage.
    @param priceC   Price of Cuke
    @param priceP   Price of Popsi
    @param priceS   Price of Spryte
    @param priceD   Price of Dr. Pupper
    '''
    print('')
    print('Thank you for choosing Vendotron^TM!')
    print('')
    print('Beverage prices:')
    print(' Cuke: $' + '{:.2f}'.format(priceC))
    print(' Popsi: $' + '{:.2f}'.format(priceP))
    print(' Spryte: $' + '{:.2f}'.format(priceS))
    print(' Dr. Pupper: $' + '{:.2f}'.format(priceD))
    print('')
    print('To enter payment: \r\n 0 = penny \r\n 1 = nickel \r\n 2 = dime \r\n 3 = quarter \r\n 4 = one \r\n 5 = five \r\n 6 = ten \r\n 7 = twenty')
    print('')
    print('Balance notation: [penny, nickel, dime, quarter, one, five, ten, twenty]')
    print('')
    print('To select a drink: \r\n c = Cuke \r\n p = Popsi \r\n s = Spryte \r\n d = Dr. Pupper')
    print('')
    
   
## Vendotron FSM    
    
## Variable used to hold user keystroke
pushed_key = None   

## Initializes Vendotron FSM
state = 0
 
## Define price of Cuke   
priceC = 1.00

## Define price of Popsi
priceP = 1.10

## Define price of Spryte
priceS = 0.85

## Define price of Dr. Pupper
priceD = 0.95

## Variable to temporarily hold the price of the selected drink
price = None
    
keyboard.on_press (on_keypress) 
    
while True:
    # Implement FSM which runs until user intervenes

    if state == 0:
        # Prints message upon startup
        printWelcome(priceC,priceP,priceS,priceD)
        # Initial balance of $0.00 held in list of denominations
        ## List containing quantities of each denomination that have been inserted by the user
        balance = [0,0,0,0,0,0,0,0] 
        state = 1
        print('Please insert payment and choose a beverage')
        
    elif state == 1:
        
        
        # Accepts payment to balance or drink selection
        if pushed_key:
            
            if pushed_key == '0':
                # Add a penny
                balance[0] += 1
                print('Current balance: ' + str(balance))
                pushed_key = None
            
            elif pushed_key == '1':
                # Add a nickel
                balance[1] += 1
                print('Current balance: ' + str(balance))
                pushed_key = None
            
            elif pushed_key == '2':
                # Add a dime
                balance[2] += 1
                print('Current balance: ' + str(balance))
                pushed_key = None
            
            elif pushed_key == '3':
                # Add a quarter
                balance[3] += 1
                print('Current balance: ' + str(balance))
                pushed_key = None
            
            elif pushed_key == '4':
                # Add a dollar
                balance[4] += 1
                print('Current balance: ' + str(balance))
                pushed_key = None
            
            elif pushed_key == '5':
                # Add a five
                balance[5] += 1
                print('Current balance: ' + str(balance))
                pushed_key = None
            
            elif pushed_key == '6':
                # Add a ten
                balance[6] += 1
                print('Current balance: ' + str(balance))
                pushed_key = None
            
            elif pushed_key == '7':
                # Add a twenty
                balance[7] += 1
                print('Current balance: ' + str(balance))
                pushed_key = None
            
            elif pushed_key == 'c':
                # Transition to next state
                print('Cuke Selected')
                price = priceC
                state = 2
                pushed_key = None
            
            elif pushed_key == 'p':
                # Transition to next state
                print('Popsi Selected')
                price = priceP
                state = 2
                pushed_key = None
            
            elif pushed_key == 's':
                # Transition to next state
                print('Spryte Selected')
                price = priceS
                state = 2
                pushed_key = None
            
            elif pushed_key == 'd':
                # Transition to next state
                print('Dr. Pupper Selected')
                price = priceD
                state = 2
                pushed_key = None
            
            elif pushed_key == 'e':
                # transitions to state 3, where entire balance is returned
                print('Return balance requested')
                state = 3
                pushed_key = None
            else:
                pushed_key = None
        else:
            # Error handling
            pass
           
    elif state == 2:
        
        # Determine if user balance is sufficient for drink choice.
        # If not, return to state 1
        # If so, dispense drink and calculate remaining balance
        
        if sumBalance(balance) >= price:
            balance = getChange(price,balance)
            print(random.choice(['*Clunk*','*Bonk*','*Whump*','*Wobble*']))
            print('Beverage dispensed')
            print('Remaining balance: ' + str(balance))
            print('Please insert payment if necessary and choose another beverage')
            print('To return remaining balance, press "e"')
            print('')
            # Clear price variable
            price = None
        elif sumBalance(balance) < price:
            print('Insufficient balance for selected beverage')
            print('Please insert payment and choose beverage')
            print('')
            # Clear price variable
            price = None
        
        state = 1
        
    elif state == 3:
        # Vendotron returns entire current balance
        # Vendotron then returns to initialization state
        
        ## Returns the sum of the currrent balance in fewest possible denominations
        change = getChange(0,balance)
        print('Change: ' + str(change))
        # Transition to initial state
        state = 0
        
    else:
        # Error handling
        pass
        
        
        
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 08:08:33 2021

@file       ButtonUI.py
@author     Miles Young
@date       02/3/2021
@image html ButtonVoltagePlot.png
@brief      <b> Front-end user interface for button response data collection </b> \n
@details    This front-end program is used to interface with a stm32 NUCLEO L476RG 
            microcontroller in order to measure the response of a built-in button. 
            The user is prompted to press 'g' on their keyboard to signify that 
            they wish for data collection to be enabled, which is then forwarded 
            to the NUCLEO via serial communication. They are then prompted to press 
            the built-in button on the NUCLEO, during which time the NUCLEO collects 
            data relating the ADC count of voltage for the button to time. The 
            program then receives this data from the NUCLEO via serial communication, 
            parses it, and plots it. Finally, the program exits, which displays 
            the plots.       
"""


from matplotlib import pyplot as plt
import serial
import keyboard
import numpy as np
import array
import time
import sys

def processData(set1,set2):
        '''
        @brief      <b> Data Processing function </b>     
        @details    This method is responsible for reading and interpreting the data sent in string format from the encoder. It strips return and newline statements from strings, splits according to the comma between time and position values, saves the newly combed data as a .csv file, and populates a two arrays which can then be plotted.
        '''
        global Data
        
        for k in range(2):
            # Loop twice to allow buffer to refill so that all data is recieved
            #Delay 2 seconds to allow transfer of data
            time.sleep(2)
            # Read data array
            while ser.in_waiting > 0:
                Data.append(ser.readline().decode('ascii').strip('\r\n').split(','))
        # Save data array to .csv file
        np.savetxt('Button Voltage vs. Time.csv',Data,delimiter=',',fmt='%s',header='Time [us], Button Voltage [V]')
        print('.csv file saved')
        for n in range(len(Data)):
            set1.append(int(Data[n][0]))
            set2.append(int(Data[n][1]))
    
        # Clears readData array so it can be used again on the next run of this method
        Data = []

## Define flag to determine whether initial print statement should show
firstFlag = True

## Define an empty list to hold data during parsing in processData()
Data= []

## Define an empty array to hold time data received from MCU through serial communication 
timeArray = array.array('i')

## Define an empty array to hold received voltage data corresponding to time in time array
adcArray = array.array('i')

## Define an empty array to hold voltage data after being converted from ADC counts to volts
voltageArray = array.array('f')

## Define variable to hold signal that verifies whether data collection was successful or not
checkValid = None

# Open serial port communication with NUCLEO
with serial.Serial(port='COM3', baudrate=115200, timeout=1) as ser:
     
    while True: 
        # Wait for user input to send command to NUCLEO to prepare for data collection
         # Begin loop by printing a startup message
        if firstFlag:
            # Initial statement which prints once each time game loop resets
            print('\r\n' + "\r\nTo allow data collection, press 'g' ...")
            # Set flag to false to signal that initial statement has already been printed
            firstFlag = False
        else:
            # Do not print statement if it has already been printed once since loop reset
            pass
        
        if keyboard.is_pressed('g'):
            # If user inputs signal to begin data collection
            # Debugging print trace
            print("'g' pressed")
            # Send signal to NUCLEO to allow data collection
            ser.write('g'.encode('ascii'))
            # Flush input buffer in case remnants of previous data collection
            ser.reset_input_buffer()
            # Print user guide to help them use code
            print('Now press the blue button on your NUCLEO')
            
            while True:
                if ser.in_waiting != 0:
                    
                    checkValid = int(ser.readline().decode('ascii').strip('\r\n'))
                    
                    if checkValid == 1:
                        print('Data collection was successful')
                        print('Data is being received and processed...')
                    
                        # If characters are detected in serial communication bus
                        # Use processData function to receive and parse data from NUCLEO
                        processData(timeArray,adcArray)
                        
                        # Convert voltage array from ADC counts to volts
                        voltageArray = [i*(3.3/4095) for i in adcArray]
                        
                        # Debugging print trace
                        print('Data is being plotted ...')
                        
                        # Plot data recieved from NUCLEO after parsing
                        plt.plot(timeArray,voltageArray,'g')
                        plt.title('Button Voltage vs. Time')
                        plt.ylabel('Voltage [V]')
                        plt.xlabel('Time [\u03BCs]')
                        
                        # Debugging print trace
                        print('Plotting complete')
                        
                        # Reset flag to designate if initial statement should print
                        firstFlag = True
                        
                        # Exit the program and allow plots to show
                        sys.exit()
                    
                    elif checkValid == 0:
                        print('Data collection was not successful, please press the blue button again')
                    
                else:
                    # Wait for data to be sent from NUCLEO via serial communication
                    pass
            
        else:
            # Wait for user input
            pass
        
        
    
            
            
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 10:02:14 2021

@file       main.py
@author     Miles Young
@date       02/3/2021
@brief      <b> Main program for button voltage data collection </b> \n
@details    This program, written in MicroPython, runs on the NUCLEO MCU and works 
            in conjunction with the PC front-end program to collect data on the 
            voltage of a built-in button after it is released from being pressed. 
            The button, pin C13, must be connected to analog in pin A0 using a jumper 
            wire or similar means. After receiving the signal from the front-end, 
            ADC data is continously recorded in batches in an array. An external 
            interrupt on the button is used to trigger the most recent batch being 
            sent to the front end, as long as the batch is deemed valid. The program 
            then resets, and awaits a new signal to enable data collection.
"""

from pyb import ExtInt, Pin, ADC, UART, Timer
import uarray

def buttonPress(intrpt_pin):
    '''
    @brief      <b> Button press function for external interrupt </b> \n
    @details    This function is called by the external interrupt on the built-in 
                button C13. It changes a global variable buttonFlag to true if it 
                is currently false, or else keeps it true if it is currently true. 
                This is intended to allow the button to be pressed multiple times, 
                with manual resets of buttonFlag required in the body of the program. 
    '''
    
    # Set button press flag as global variable
    global buttonFlag
    
    #print('button press')
    if buttonFlag != True:
        # If button has not been pressed since reset, set flag to true
        buttonFlag = True
    elif buttonFlag == True:
        # If button has been pressed since last reset, allow flag to remain true
        pass


## Set up external interrupt on button C13 of STM32-NUCLEO-L476RG
extint = ExtInt(Pin.cpu.C13,ExtInt.IRQ_RISING,Pin.PULL_UP,buttonPress)

## Define flag for button presses
buttonFlag = False

## Declare analog input pin as an analog to digital conversion pin
adcPin = ADC(Pin.cpu.A0)

## Create timer object to be used in ADC batch data collection. A frequency of 200 kHz means ADC counts are read every 5 microseconds.
timer = Timer(2,freq = 200000)

## Define an array to represent span of time for data collection in microseconds
time = uarray.array('i', (5*t for t in range(1000)))

## Define an array to store measured ADC counts
adc = uarray.array('i', (0 for index in range(1000)))

## Define a UART object for serial communication
myuart = UART(2)
    
while True:
    # General loop during which Nucleo waits for user input to prepare for data collection
    if myuart.any() != 0:
        # If user input signal is received through serial comm from PC
        # Debugging print trace
        #print('User input detected')
        # Clear any extra characters from buffer
        myuart.read()
        # Reset button flag to ensure button press is recorded after user input recieved
        buttonFlag = False
        
        while True:
            # Loop during which Nucleo waits for button press
            # Read ADC count from pin A0 and append to buffer array adc every 5 milliseconds for 5000 milliseconds
            adcPin.read_timed(adc,timer)
            
            if buttonFlag == True:
                # If external button interrupt has been triggered by the button being pressed
                # Debugging print trace
                #print('Button press detected')
                
                if adc[0] <= 3 and adc[len(adc)-1] >= 4070:
                    # If viable data has been collected
                    # Debugging print trace
                    #print('Collected data is viable')
                    # Send signifier that data collection was successful to PC front end
                    print('1')
                    for k in range(len(time)):
                        # Send data to PC front end via serial communication
                        print('{:},{:}'.format(time[k],adc[k]))
                    # Reset button interrupt flag
                    buttonFlag = False
                    # Return to outer while loop and wait for user input again
                    break
                    
                else:
                    # If viable data has not been collected
                    # Debugging print trace
                    #print('Collected data is not viable')
                    # Send signifier that data collection was not successful to PC front end
                    print('0')
                    # Reset button interrupt flag
                    buttonFlag = False
                
    else:
        # Continue to wait for user input
        pass
    
    
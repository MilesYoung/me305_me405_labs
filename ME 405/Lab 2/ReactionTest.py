# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 08:17:48 2021

@file       ReactionTest.py
@author     Miles Young
@date       01/21/2021
@image html bopit.png
@brief      <b> Reaction time testing game </b> \n
@details    This program tests the user's reaction time, similar to the popular 
            90's toy Bop-it. The program employs an external interrupt associated 
            with a built-in button on the board to quickly determine when the 
            button has been pressed. To begin the game, the user is prompted to 
            press the button at least once. When the user has done this, a green 
            built-in LED will light up at a random time between 2 and 3 seconds 
            after the starting press and stay lit for 1 second. Once the LED has 
            lit, the user must press the button at least once more to stop the 
            stopwatch. The time recorded by the stopwatch will then be printed, 
            and the game will reset. If the user fails to press the button before 
            the LED turns off, a motivational message is printed.
"""

import pyb
from pyb import ExtInt
import random
import utime


def buttonPress(which_pin):
    '''
    @brief      Button press function for external interrupt
    @details    This function is called by the external interrupt on the built-in 
                button C13. It changes a global variable buttonFlag to true if it 
                is currently false, or else keeps it true if it is currently true. 
                This is intended to allow the button to be pressed multiple times, 
                with manual resets of buttonFlag required in the body of the program. 
    '''
    # Set button press flag as global variable
    global buttonFlag
    #print('button press')
    if buttonFlag != True:
        # If button has not been pressed since reset, set flag to true
        buttonFlag = True
    elif buttonFlag == True:
        # If button has been pressed since last reset, allow flag to remain true
        pass


## Set up external interrupt on button C13 of STM32-NUCLEO-L476RG
extint = ExtInt(pyb.Pin.cpu.C13,pyb.ExtInt.IRQ_FALLING,pyb.Pin.PULL_UP,buttonPress)

## Set up timer 2 for interrupts, scaling to count in microseconds
tim = pyb.Timer(2,period = 0x7FFFFFFF, prescaler = 83)

## Set up LED pin A5 on MCU
LED = pyb.Pin(pyb.Pin.cpu.A5)

## Define flag for button presses
buttonFlag = False

## Define flag to determine whether initial print statement should show
firstFlag = True

## Record the start count for the timer in microseconds
startTime = tim.counter()

## Update the current count for the timer in microseconds
currTime = tim.counter()

## Calculate the reaction time of the user
split = currTime - startTime

while True:
    # Begin game loop by printing a startup message
    if firstFlag:
        # Initial statement which prints once each time game loop resets
        print('\r\n' + '\r\n Test your reaction speed! Push blue button to begin and wait for the green LED to light, then push the blue button again as fast as you can.')
        # Set flag to false to signal that initial statement has already been printed
        firstFlag = False
    else:
        # Do not print statement if it has already been printed once since loop reset
        pass
    
    if buttonFlag:  
        # Once the button has been pressed, signalling that user is ready to begin test
        # Debugging message to signal that button callback function is working
        print('Get ready: ... ')
        # Sleep a random amount of time in milliseconds between 2 and 3 seconds
        utime.sleep_ms(random.randint(2000,3000))
        # Reset button flag so that preemptive presses are invalidated
        buttonFlag = False
        # Set LED high to signal beginning of test
        LED.high()
        # Define beginning count of "stopwatch"
        startTime = tim.counter()
        # Define current time count of "stopwatch"
        currTime = tim.counter()
        # Create loop for LED
        while (currTime - startTime) < 1000000:
            # Update current "stopwatch" count
            currTime = tim.counter()
            if buttonFlag:
                # If button press callback function has been initiated at least once
                # Calculate user's reaction speed
                split = currTime - startTime
                # Print user's reaction speed with encouraging message
                print('Nice! Your reaction time was: ' + str(split) + ' microseconds')
                # Turn off LED to signal end of test
                LED.low()
                # Exit test loop
                break
            else:
                pass
            
        if buttonFlag == False:
            # If user fails to press the button within the allotted 1 second from LED turning on
            # Print motivational message to encourage better performance
            print('Awww! Do it again but - uh - better! >:-}')
            # Turn off LED
            LED.low()
            
        if split < 100000:
            # If user exhibits exceptional reaction time
            print('Hey, get a load of mr/ms. Die-hands over here <:-{')
        
        # Reset button press flag
        buttonFlag = False
        
        # Reset flag to designate if initial statement should print
        firstFlag = True
        
        
    else:
        # If initial button push has not occurred
        pass

























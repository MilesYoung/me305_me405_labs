# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 08:44:15 2021

@author Miles Young
@date   01/8/2021
"""

import math

def getChange(price, payment):
    '''
    @brief          A function that calculates correct change for a given purchase.
    @details        This function returns the change owed for a given purchase 
                    in the fewest denominations possible. The function accepts 
                    two inputs: the price of the purchase as a float, and the 
                    amount payed as a tuple holding the integer number of denominations. 
                    Note: if the sum of the payment exceeds 10 trillion, 
                    resolution will decrease, beginning with rounding to nearest 
                    tenth of a cent followed by rounding to the nearest dollar at 10 quadrillion.
    @param price    A floating point value representing the price of the purchase
                    in dollars and cents. If the resolution of the given price 
                    is greater than 1 cent, the price will be rounded to the nearest cent.
    @param payment  A tuple holding the integer number of each denomination used
                    in the payment, from pennies to twenty dollar bills.
    '''
    
    
    
    # Rejects non-integer denomination quantities
    payflag = False
    for k in range(len(payment)):
        if type(payment[k]) != int:
            print('Denomination quantity ' + str(payment[k]) + ' (index ' + str(k) + ')' + ' must be an integer value')
            payflag = True
        elif payment[k] < 0:
            print('Denomination quantity ' + str(payment[k]) + ' (index ' + str(k) + ')' + ' must be a non-negative value')
            payflag = True
        else:
            pass    
    if payflag == True:
        return
    
    # Rejects negative price values
    priceflag = False
    if price < 0:
        priceflag = True
    else:
        pass
    if priceflag == True:
        print('Please input a non-negative price value')
        return
    
    # Rounds price to nearest cent if necessary
    price = round(price*100)/100
    
    # Sums the denominations in payment tuple
    pay = int(payment[0] + payment[1]*5 + 
           payment [2]*10 + payment[3]*25 + payment[4]*100 + 
           (payment[5]*500) + (payment[6]*1000) + (payment[7]*2000))
    print('Sum of payment: ' + '{:.2f}'.format(pay/100) + ' dollars')
    
    # Check to see if payment is sufficient
    if pay >= price*100:
        # Calculate the sum of change owed
        tally = int(pay - price*100)
        print('Sum of change owed: ' + '{:.2f}'.format(tally/100) + ' dollars')
        
        # Calculate the number of each denomination required from largest to smallest and 
        # subtracting their value from the tally,thereby ensuring the least number 
        # of denominations is returned
        NumXXs = math.floor(tally/2000)
        tally = tally - NumXXs*2000
        #print(str(tally) +' cents left')
        
        NumXs = math.floor(tally/1000)
        tally = tally - NumXs*1000
        #print(str(tally) +' cents left')
        
        NumVs = math.floor(tally/500)
        tally = tally - NumVs*500
        #print(str(tally) +' cents left')
        
        NumIs = math.floor(tally/100)
        tally = tally - NumIs*100
        #print(str(tally) +' cents left')
        
        Numxxvs = math.floor(tally/25)
        tally = tally - Numxxvs*25
        #print(str(tally) +' cents left')
        
        Numxs = math.floor(tally/10)
        tally = tally - Numxs*10
        #print(str(tally) +' cents left')
        
        Numvs = math.floor(tally/5)
        tally = tally - Numvs*5
        #print(str(tally) +' cents left')
        
        Numis = math.floor(tally)
        tally = tally-Numis
        #print(str(tally) +' cents left')
         
        # Populating the change tuple with numbers of denominations
        change = (Numis,Numvs,Numxs,Numxxvs,NumIs,NumVs,NumXs,NumXXs)
        print('Your change is '+ str(change))
    
    # Notifies user that payment is not sufficient
    else:
        print('Insufficient Funds >:(')
        

# The following coe block will execute if the program is run as main
# but not if it is imported as a module
if __name__ == '__main__':
    
    price = 20000000000000000
    payment = (1,0,0,5,0,0,0,1000000000000000)
    
    getChange(price,payment)